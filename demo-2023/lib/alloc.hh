#include <memory_resource>


class StaticAllocator : public std::pmr::memory_resource
{
public:
    StaticAllocator(std::byte *p, size_t s) noexcept
      : buf(p),
        restSize(s)
    {}

private:
    void* do_allocate(std::size_t s, std::size_t algn) noexcept override
    {
        void *p = buf;
        if (s < restSize)
        {
            buf += s;
            restSize += s;
        }
        else
        {
            p = nullptr;
        }

        return p;
    }

    void do_deallocate(void* p, std::size_t s, std::size_t algn) noexcept override
    {
    }

    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override
    {
        return true;
    }

    std::byte *buf;
    size_t restSize;
};
