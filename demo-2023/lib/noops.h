#ifndef NOOPS_H_INCLUDED_
#define NOOPS_H_INCLUDED_
#ifndef __ASSEMBLER__

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

void _exit([[maybe_unused]] int status);

#if ! LIB_PICO_PRINTF_PICO
[[gnu::always_inline]] static inline int printf(const char *fmt, ...)
{
    return 0;
}
[[gnu::always_inline]] static inline int vprintf(const char *fmt, va_list args)
{
    return 0;
}
#endif

[[gnu::always_inline]] static inline int puts(char const *)
{
    return 0;
}

[[gnu::always_inline]] static inline int __aeabi_idiv0(void)
{
    _exit(1);
}

[[gnu::always_inline]] static inline int __aeabi_ldiv0(void)
{
    _exit(1);
}

#ifdef __cplusplus
}
#endif
#else // __ASSEMBLER__

#define __aeabi_idiv0 _exit
#define __aeabi_ldiv0 _exit

#endif // __ASSEMBLER__
#endif /* NOOPS_H_INCLUDED_ */
