// simple debug output via UART for RP2040

#include "uart.hh"

#include "systraits.hh"

#include "rp2040.h"
#include "rp_regs.h"
#include "helpers.hh"
#include <port-header.hh>

#include <cstring> // memcpy
#include <array>


namespace
{
constexpr uint32_t PeriFreq = 126'000'000;
constexpr uint32_t BaudRate = 115200;

template <UartDev dev> struct UartTraits;

template <>
struct UartTraits<UartDev::uart0>
{
    typedef cppos::port::helpers::GpioFunction GpioFunction;

    static constexpr Reg<UART0_Type> regs{UART0_BASE};
    static constexpr IRQn_Type irqNo = UART0_IRQ_IRQn;
    static constexpr uint32_t txGpio = 0;
    static constexpr GpioFunction txGpioFunc = GpioFunction::uart;
    static constexpr uint32_t rxGpio = 1;
    static constexpr GpioFunction rxGpioFunc = GpioFunction::uart;
    static constexpr uint32_t resetMsk = RESETS_RESET_uart0_Msk;
};

template <>
struct UartTraits<UartDev::uart1>
{
    typedef cppos::port::helpers::GpioFunction GpioFunction;

    static constexpr Reg<UART0_Type> regs{UART1_BASE};
    static constexpr IRQn_Type irqNo = UART1_IRQ_IRQn;
    static constexpr uint32_t txGpio = 8;
    static constexpr GpioFunction txGpioFunc = GpioFunction::uart;
    static constexpr uint32_t rxGpio = 9;
    static constexpr GpioFunction rxGpioFunc = GpioFunction::uart;
    static constexpr uint32_t resetMsk = RESETS_RESET_uart1_Msk;
};

// we need to make sure that both uart objs are correctly aligned
constexpr size_t uartSize = (((sizeof(Uart<UartDev::uart0>) + 3) / 4) * 4);
constexpr size_t uartSpaceSize =
    uartSize * (cppos::board_traits::useUart1 ? 2 : 1);
alignas(4) std::array<std::byte, uartSpaceSize> uartSpace;

struct CreateUarts
{
    CreateUarts()
    {
        new (uartSpace.data()) Uart<UartDev::uart0>;
        if constexpr (cppos::board_traits::useUart1)
        {
            new (uartSpace.data() + uartSize) Uart<UartDev::uart1>;
        }
    }
} uartCreator;
} // unnamed namespace


Uart<UartDev::uart0> &dbgOut
    = *(reinterpret_cast<Uart<UartDev::uart0> *>(uartSpace.data()));
Uart<UartDev::uart1> &uart1
    = *(reinterpret_cast<Uart<UartDev::uart1> *>(uartSpace.data() + sizeof(Uart<UartDev::uart0>)));

template <UartDev dev>
Uart<dev>::Uart()
  : txState(idle)
{
    reset();
}

template <UartDev dev>
Uart<dev>::~Uart()
{
    disableIrq(UartTraits<dev>::irqNo);
    // we disable everything
    UartTraits<dev>::regs.ptr()->UARTCR = 0;
}

template <UartDev dev>
void Uart<dev>::write(void const *data, uint16_t len)
{
    if (txState == txBusy)
    {
        return;
    }
    txState = txBusy;

    memcpy(buf, (void *)data, len);

    txCurrent = 0;
    txLen = len;
    if (txLen != 0)
    {
        sendNext(*this);
        if (txState == txBusy)
        {
            // enable interrupt
            UartTraits<dev>::regs.ptrSet()->UARTIMSC = UART0_UARTIMSC_TXIM_Msk;
        }
    }
}

template <UartDev dev>
bool Uart<dev>::done()
{
    // on this chip we don't get an interrupt when the FIFO is empty
    // and we don't want to start a timer
    // but we can look at the status register
    //return UartTraits<dev>::regs.ptr()->UARTFR_b.TXFE;
    //return not UartTraits<dev>::regs.ptr()->UARTFR_b.BUSY;

    // we probably only want to know if we can start the next write
    // if we call this with interrupts disabled, we use this as busy loop
    //if (txState != txBusy)
    if ((txState != txBusy) && !(UartTraits<dev>::regs.ptr()->UARTFR_b.BUSY))
    {
        return true;
    }

    if (!cppos::port::interrupts_enabled()
        && !(UartTraits<dev>::regs.ptr()->UARTFR_b.TXFF))
    {
        sendNext(*this);
    }
    return false;
}

template <UartDev dev>
void Uart<dev>::reset()
{
    // baud rate divisor = uart clock / (16 x baud rate)
    // the divisor has a 6-bit fractional part so we multiply by 64 for integer
    // uart clock is peri clock
    constexpr uint32_t BrDivX64 = (PeriFreq * 4) / BaudRate;

    enablePeripheral(UartTraits<dev>::resetMsk);
    UartTraits<dev>::regs.ptr()->UARTIBRD = BrDivX64 >> 6;
    UartTraits<dev>::regs.ptr()->UARTFBRD = BrDivX64 & 0x3f;

    // 8N1 and FIFO enable
    UartTraits<dev>::regs.ptr()->UARTLCR_H = (3 << UART0_UARTLCR_H_WLEN_Pos)
        | UART0_UARTLCR_H_FEN_Msk;
    // default FIFO trigger should be ok
    // for now transmit only
    UartTraits<dev>::regs.ptr()->UARTCR = UART0_UARTCR_UARTEN_Msk | UART0_UARTCR_TXE_Msk;

    gpioFuncAssign(UartTraits<dev>::txGpio, UartTraits<dev>::txGpioFunc);
    gpioFuncAssign(UartTraits<dev>::rxGpio, UartTraits<dev>::rxGpioFunc);

    // disable all UART internal interrupts
    UartTraits<dev>::regs.ptr()->UARTIMSC = 0;
    // but enable the overall interrupt here
    enableIrq(UartTraits<dev>::irqNo);
}

template <UartDev dev>
void sendNext(Uart<dev> &uart)
{
    // clear interrupt
    //auto ptr = UartTraits<dev>::regs.ptr();
    UartTraits<dev>::regs.ptrSet()->UARTICR = UART0_UARTIMSC_TXIM_Msk;

    //UartTraits<dev>::regs.ptr()->UARTDR = '.';
    while ((uart.txCurrent != uart.txLen)
           && not UartTraits<dev>::regs.ptr()->UARTFR_b.TXFF)
    {
        UartTraits<dev>::regs.ptr()->UARTDR = uart.buf[uart.txCurrent++];
    }

    if (uart.txCurrent == uart.txLen)
    {
        if (uart.txLen != 0)
        {
            // disable interrupt
            UartTraits<dev>::regs.ptrClr()->UARTIMSC = UART0_UARTIMSC_TXIM_Msk;
            // now we can allow the next write() to overwrite our buffer
            uart.txState = Uart<dev>::txDone;
        }
        else
        {
            uart.txState = Uart<dev>::idle;
        }
        uart.txCurrent = uart.txLen = 0;
    }
}

template class Uart<UartDev::uart0>;
template class Uart<UartDev::uart1>;

extern "C"
{
void UART0_IRQHandler()
{
    sendNext(dbgOut);
}

void UART1_IRQHandler()
{
    sendNext(uart1);
}
} // extern "C"
