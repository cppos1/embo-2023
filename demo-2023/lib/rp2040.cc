// helpers for RP2040

#include "rp2040.hh"

#include "helpers.hh"

#include <port-header.hh>

#include <rp2040.h>

#include <cstddef>
#include <array>

#define SHIFT_VALUE(x,v) (x##_##v << x##_Pos)

namespace
{
constexpr size_t core1StackSize = 8000;
std::byte core1Stack[core1StackSize];
} // unnamed namespace

namespace rp2040
{
void clocks_init()
{
    // it looks like writing to the bitfields is generally not a good idea
    // it causes a read-modify-write while often we want a write only
    // reading is fine
    // XOSC procedure from datasheet
    XOSC->CTRL = SHIFT_VALUE(XOSC_CTRL_FREQ_RANGE, 1_15MHZ);
    XOSC->STARTUP = 47;
    XOSC_SET->CTRL = SHIFT_VALUE(XOSC_CTRL_ENABLE, ENABLE);
    while (XOSC->STATUS_b.STABLE == 0)
        ;

    CLOCKS->CLK_REF_CTRL = SHIFT_VALUE(CLOCKS_CLK_REF_CTRL_SRC, xosc_clksrc);
    while (CLOCKS->CLK_REF_SELECTED != (1 << CLOCKS_CLK_REF_CTRL_SRC_xosc_clksrc))
        ;
    CLOCKS->CLK_SYS_CTRL = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_SRC, clk_ref);
    while (CLOCKS->CLK_SYS_SELECTED != (1 << CLOCKS_CLK_SYS_CTRL_SRC_clk_ref))
        ;


    // SYS PLL 126MHz from datasheet
    enablePeripheral(RESETS_RESET_pll_sys_Msk);
    PLL_SYS->CS = 1 << PLL_SYS_CS_REFDIV_Pos;
    PLL_SYS->FBDIV_INT = 63;
    PLL_SYS_CLR->PWR = PLL_SYS_PWR_PD_Msk | PLL_SYS_PWR_VCOPD_Msk;
    while (PLL_SYS->CS_b.LOCK == 0)
        ;
    PLL_SYS->PRIM = (6 << PLL_SYS_PRIM_POSTDIV1_Pos) |
        1 << (PLL_SYS_PRIM_POSTDIV2_Pos);
    PLL_SYS_CLR->PWR = 1 << PLL_SYS_PWR_POSTDIVPD_Pos;

    CLOCKS->CLK_SYS_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_AUXSRC, clksrc_pll_sys);
    CLOCKS->CLK_SYS_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_SRC, clksrc_clk_sys_aux);
    while (CLOCKS->CLK_SYS_SELECTED !=
           (1 << CLOCKS_CLK_SYS_CTRL_SRC_clksrc_clk_sys_aux))
        ;

    // no need to stop, we're coming out of reset
    CLOCKS->CLK_PERI_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_PERI_CTRL_AUXSRC, clk_sys);
    CLOCKS_SET->CLK_PERI_CTRL = 1 << CLOCKS_CLK_PERI_CTRL_ENABLE_Pos;


    // we don't enable the watchdog, but we must start it
    // for the reference timer
    WATCHDOG_SET->TICK = (WATCHDOG_TICK_ENABLE_Msk |
                          (12 << WATCHDOG_TICK_CYCLES_Pos));


#if 0
    // USB PLL from datasheet
    enablePeripheral(RESETS_RESET_pll_usb_Msk);
    PLL_USB->CS = 1 << PLL_SYS_CS_REFDIV_Pos;
    PLL_USB->FBDIV_INT = 64;
    PLL_USB_CLR->PWR = PLL_SYS_PWR_PD_Msk | PLL_SYS_PWR_VCOPD_Msk;
    while (PLL_USB->CS_b.LOCK == 0)
        ;
    PLL_USB->PRIM = (4 << PLL_SYS_PRIM_POSTDIV1_Pos) |
        4 << (PLL_SYS_PRIM_POSTDIV2_Pos);
    PLL_USB_CLR->PWR = 1 << PLL_SYS_PWR_POSTDIVPD_Pos;

    // no need to stop, we're coming out of reset
    CLOCKS->CLK_USB_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_USB_CTRL_AUXSRC, clksrc_pll_usb);
    CLOCKS_SET->CLK_USB_CTRL = 1 << CLOCKS_CLK_USB_CTRL_ENABLE_Pos;
#endif
}

void start_core1(void (*f)())
{
    // from datasheet
    // we use the same irq table as core0
    std::array cmd_sequence =
    {
        uintptr_t(0), uintptr_t(0), uintptr_t(1),
        uintptr_t(PPB->VTOR),
        uintptr_t(core1Stack) + core1StackSize,
        uintptr_t(f)
    };
    size_t seq = 0;
    do {
        uintptr_t cmd = cmd_sequence[seq];
        // Always drain the READ FIFO (from core 1) before sending a 0
        if (!cmd) {
            while (SIO->FIFO_ST_b.VLD)
            {
                (void) SIO->FIFO_RD;
            }
            // Execute a SEV as core 1 may be waiting for FIFO space via WFE
            __SEV();
        }
        // push next cmd
        while (not SIO->FIFO_ST_b.RDY)
            ;
        SIO->FIFO_WR = cmd;
        __SEV();

        // read response
        while (not SIO->FIFO_ST_b.VLD)
            ;
        uint32_t response = SIO->FIFO_RD;

        // Move to next state on correct response (echo-d value) otherwise start over
        seq = cmd == response ? seq + 1 : 0;
    } while (seq < cmd_sequence.size());

    // read any leftovers from the FIFO
    while (SIO->FIFO_ST_b.VLD)
    {
        [[maybe_unused]] uint32_t val = SIO->FIFO_RD;
    }
    enableIrq(SIO_IRQ_PROC0_IRQn);
    cppos::port::enable_interrupts();
}
} // namespace rp2040
