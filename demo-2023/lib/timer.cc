// simple timer implementation

#include "timer.hh"

#include <rp2040.hh>
#include <helpers.hh>

#include <rp2040.h>


namespace rp2040
{
Timer::Timer(uint32_t timerNo)
  : instance(timerNo)
  ,  wakeupTime(0)
{
    enableIrq(IRQn_Type(TIMER_IRQ_0_IRQn + instance));
    events[timerNo] = this;
}

void Timer::setup(uint32_t timeout)
{
    wakeupTime = timeUs() + timeout;
    (&(TIMER->ALARM0))[instance] = wakeupTime;
    TIMER_SET->INTE = 1 << instance;
}

void Timer::fire(uint32_t timerNo)
{
    TIMER_CLR->INTR = 1 << timerNo;
    TIMER_CLR->INTE = 1 << timerNo;

    // ToDo: we could get the interrupt on the wrong core (e.g. for output)
    events[timerNo]->unblock_all();
}

cppos::event *Timer::events[4] = {nullptr, nullptr, nullptr, nullptr};
} // namespace rp2040

extern "C"
{
void TIMER0_IRQHandler()
{
    rp2040::Timer::fire(0);
}

void TIMER1_IRQHandler()
{
    rp2040::Timer::fire(1);
}

void TIMER2_IRQHandler()
{
    rp2040::Timer::fire(2);
}

void TIMER3_IRQHandler()
{
    rp2040::Timer::fire(3);
}
} // extern "C"
