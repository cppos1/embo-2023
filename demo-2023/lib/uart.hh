// simple UART class (for now output only)
#ifndef UART_HH_SEEN
#define UART_HH_SEEN

#include "helpers.hh"

#include "rp2040.h"

#include <cstdint>
#include <cstddef>

class UartImpl;

enum class UartDev : uint32_t
{
    uart0,
    uart1,
};

template <UartDev dev> class Uart;
template <UartDev dev> void sendNext(Uart<dev> &uart);

template <UartDev dev>
class Uart
{
public:
    Uart();
    ~Uart();

    void write(void const *data, uint16_t len);
    // not const, as it may be used for busy loop when interrupts are disabled
    bool done();

    void reset();

private:
    friend void sendNext<dev>(Uart<dev> &);

    enum IoState : uint8_t
    {
        idle,
        txBusy,
        txDone,
        rxBusy,
        rxDone,
    };

    static constexpr size_t maxLen = 180;

    uint8_t buf[maxLen];
    volatile IoState txState;
    uint32_t txCurrent = 0;
    uint32_t txLen = 0;
};

extern Uart<UartDev::uart0> &dbgOut;
extern Uart<UartDev::uart1> &uart1;

#endif /* UART_HH_SEEN */
