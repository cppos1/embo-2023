#ifndef RP2040_TIMER_HH_INCLUDED
#define RP2040_TIMER_HH_INCLUDED

#include <cppos/scheduler.hh>

#include <cstdint>

namespace rp2040
{
// proof of concept only
class Timer : cppos::event
{
public:
    explicit Timer(uint32_t timerNo);

    template <typename Exec>
    void waitForUs(uint32_t timeoutUs)
    {
        setup(timeoutUs);

        // this is racy for extremely short timeouts:
        // if we get the interrupt here, we'll wait forever
        // we need to split Exec::block into pre-block to set the flag
        // and the actual waiting
        Exec::block(*this);
    }

    static void fire(uint32_t timerNo);

private:
    void setup(uint32_t timeout);

    uint32_t instance;
    uint32_t wakeupTime;

    static cppos::event *events[4];
};
} // namespace rp2040
#endif /* RP2040_TIMER_HH_INCLUDED */
