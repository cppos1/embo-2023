// GPIO class for RP2040

#ifndef GPIO_HH_SEEN
#define GPIO_HH_SEEN

#include "rp2040.h"
#include "rp_regs.h"
#include "helpers.hh"


template <uint8_t port, uint8_t bit>
class GpioOut
{
public:
    GpioOut(bool initState)
    {
        static_assert(port == 0, "RP2040 only has bank 0");
        SIO->GPIO_OE_SET = 1 << bit;
        set(initState);
        gpioFuncAssign(bit, GpioFunction::sio);
    }

    void set(bool state)
    {
        if (state)
        {
            SIO->GPIO_OUT_SET = 1 << bit;
        }
        else
        {
            SIO->GPIO_OUT_CLR = 1 << bit;
        }
    }

    void toggle()
    {
        SIO->GPIO_OUT_XOR = 1 << bit;
    }
};

// type erased interface
class GpioOutIf
{
private:
    struct Base;
    template <uint8_t port, uint8_t bit> struct Derived;

public:
    template <uint8_t port, uint8_t bit>
    GpioOutIf(GpioOut<port, bit> &g)
      : gpio(new (space) Derived<port, bit>(g))
    {
    }

    void set(bool state)
    {
        gpio->set(state);
    }

    void toggle()
    {
        gpio->toggle();
    }

private:
    struct Base
    {
        virtual void set(bool state) = 0;
        virtual void toggle() = 0;
    };

    template <uint8_t port, uint8_t bit>
    struct Derived final : public Base
    {
        Derived(GpioOut<port, bit> &g)
          : gpio(g)
        {
        }

        void set(bool state) override
        {
            gpio.set(state);
        }

        void toggle() override
        {
            gpio.toggle();
        }

        GpioOut<port, bit> &gpio;
    };

    Base *gpio;
    uint8_t space[sizeof(Derived<0, 0>)];
};
#endif /* GPIO_HH_SEEN */
