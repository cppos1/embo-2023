// snapcast client

#include "sound.hh"

#include <rp2040-scheduler.hh>
#include <cppos/evqueue.hh>
#include "i2s.hh"
#include <uart.hh>
#include "gpio.hh"
#include <timer.hh>

#include "rp2040.hh"

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>

#include <array>
#include <cstdint>
#include <string_view>
#include <atomic>

#include "decode.hh"

#include "cppos/scheduler.tt"


using namespace std::literals;

cppos::EvQueueBody<WireChunk, inBufSize> inBuf;
cppos::EvQueueBody<PcmFrame, outBufSize> outBuf;


void dbgInt(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
volatile uint32_t irqCnt = 0;
volatile uint32_t lastSize = 0;
volatile uint32_t i2sIrqCnt = 0;
volatile uint32_t i2sIrqPkt = 0;
volatile uint32_t opusCnt = 0;
volatile uint32_t opusFull = 0;
volatile uint32_t opusSize = 0;

namespace
{

constexpr uint32_t i2sDataPin = 19;
constexpr uint32_t i2sBClkPin = 20;
// unused, as fixed by PIO program
//constexpr uint32_t i2sLRClkPin = i2sBClkPin + 1;

constexpr AudioConfig outSettings = { .sampleFreqHz = 48000, };
constexpr AudioI2sConfig i2sSettings =
{
    .dataOutPin = i2sDataPin,
    .bClkPin = i2sBClkPin,
};


constexpr uint32_t PeriFreq = 126'000'000;
constexpr uint32_t BaudRate = 115200;
constexpr uint32_t txPin = 8;
constexpr uint32_t rxPin = 9;


constexpr auto ledPort = 0;
constexpr uint32_t ledPin = 25;

constexpr auto ledFlashRate = 800ms;

GpioOut<ledPort, ledPin> led(true);

constexpr int printBufSize = 180;
char printBuf[printBufSize] = "Here's the board\r\n";
rp2040::Timer dbgUartWait(0);
bool uart0Disable = false;

//typedef cppos::preempt_context<0> MyExecT;
typedef cppos::exec_context MyExecT;

alignas(4) uint8_t execCtxBuf[sizeof(MyExecT) + 40];
MyExecT *ctx = nullptr;

inline void syncDbgOut(char const *str, size_t len)
{
    while (!dbgOut.done())
    {
        std::this_thread::sleep_for(1ms);
        //__NOP();
    }
    dbgOut.write(str, len);
}

inline void system_setup()
{
    cppos::port::disable_interrupts();

    // on reset all spinlocks are locked
    // unlock them
    constexpr static Reg slReg{SIO_SPINLOCK0};
    for (int i = 0; i != 32; ++i)
    {
        slReg.ptr()[i] = 1;
    }
}


void uartInit()
{
    // baud rate divisor = uart clock / (16 x baud rate)
    // the divisor has a 6-bit fractional part so we multiply by 64 for integer
    // uart clock is peri clock
    constexpr uint32_t BrDivX64 = (PeriFreq * 4) / BaudRate;
    constexpr Reg<UART0_Type> regs{UART1_BASE};

    enablePeripheral(RESETS_RESET_uart1_Msk);
    regs.ptr()->UARTIBRD = BrDivX64 >> 6;
    regs.ptr()->UARTFBRD = BrDivX64 & 0x3f;

    // 8N1, RX only
    regs.ptr()->UARTLCR_H = (3 << UART0_UARTLCR_H_WLEN_Pos)
        | UART0_UARTLCR_H_FEN_Msk;
    regs.ptr()->UARTCR = UART0_UARTCR_UARTEN_Msk | UART0_UARTCR_RXE_Msk;

    gpioFuncAssign(txPin, cppos::port::helpers::GpioFunction::uart);
    gpioFuncAssign(rxPin, cppos::port::helpers::GpioFunction::uart);

    regs.ptr()->UARTDMACR = UART0_UARTDMACR_RXDMAE_Msk;
}

void ledFlashTask()
{
    while (1)
    {
        std::this_thread::sleep_for(ledFlashRate/2);
        led.toggle();
    }
}

void printTask()
{
    syncDbgOut(printBuf, 18);
    auto bufEnd = printBuf + printBufSize;

    auto printPeriod = 5s;

    while (1)
    {
        char *end = printBuf;

        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, irqCnt).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, opusCnt).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, opusSize).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, opusFull).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, i2sIrqCnt).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, i2sIrqPkt).ptr;
        *(end++) = '\r';
        *(end++) = '\n';
        syncDbgOut(printBuf, end - printBuf);
    }
}
} // unnamed namespace


// global functions for UART and I2S drivers
// this is called from ISR, don't sleep!
void dbgInt(int line, unsigned val)
{
    auto bufEnd = printBuf + printBufSize;
    char *end = printBuf;

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';
    dbgOut.write(printBuf, end - printBuf);
}

void dbgIntSync(int line, unsigned val)
{
    uart0Disable = true;
    while (!dbgOut.done()) ;
    uart0Disable = false;
    dbgInt(line, val);
    uart0Disable = true;
    while (!dbgOut.done()) ;
    uart0Disable = false;
}


void pushChunk(WireChunk const &fr)
{
    ++opusCnt;
    cppos::EvQueuePush<WireChunk, inBufSize, MyExecT> opusQ(inBuf);
    // for forwarding ref we can't have const
    auto &chunk = const_cast<WireChunk &>(fr);
    if (opusQ.try_push(chunk) == cppos::queue_op_status::full)
    {
        ++opusFull;
    }
    else
    {
        opusSize += chunk.size;
    }
}

bool pullPcmFrame(PcmFrame &fr)
{
    ++i2sIrqCnt;
    cppos::EvQueuePull<PcmFrame, outBufSize, MyExecT> pcmQ(outBuf);

    if (pcmQ.try_pop(fr) == cppos::queue_op_status::empty)
    {
        return false;
    }
    ++i2sIrqPkt;

    return true;
}


extern "C"
{
void SystemInit()
{
    rp2040::clocks_init();

    // we enable the pins here, as we're going to need them anyways
    enablePeripheral(RESETS_RESET_io_bank0_Msk | RESETS_RESET_pads_bank0_Msk);

    // we need the PIOs and DMA
    enablePeripheral(RESETS_RESET_pio0_Msk | RESETS_RESET_pio1_Msk);
    enablePeripheral(RESETS_RESET_dma_Msk | RESETS_RESET_busctrl_Msk);

    /*
     * Missing:
     *
     * - ROM functions
     *   - possibly use pico_mem_ops and pico_bit_ops from SDK
     */
}

void error_handler()
{
    while (true)
    {
    }
}

} // extern "C"


int main()
{
    system_setup();

    syncDbgOut(printBuf, 18);

    i2sSetup(&i2sSettings, &outSettings);
    i2sStart();

    uartInit();
    uartDmaSetup();
    uartStart();

    namespace exec = std::execution;
    ctx = new (execCtxBuf) MyExecT;

    // we have a bug if the task is a function pointer
    //exec::execute(ctx->scheduler<1, 3840>(), [] { receive(); });
    exec::execute(ctx->scheduler<2, 880>(), [] { printTask(); });
    exec::execute(ctx->scheduler<10, 440>(), [] { ledFlashTask(); });

    // opus decoder requires a pretty big stack
    exec::execute(ctx->scheduler<3, 6800>(), [] { decode(); });
    //exec::execute(ctx->scheduler<3, 4800>(), [] { dummyDecode(); });

    // Start everything
    cppos::startContexts();

    /* we should never get here! */
    return 0;
}
