#ifndef DECODE_HH_INCLUDED
#define DECODE_HH_INCLUDED

#include "sound.hh"

//#include <opus_types.h>

//constexpr opus_int32 sampleRate = 48000; // 1 sample ~= 20.83us
constexpr int channels = 2;

[[noreturn]] void decode();
#endif /* DECODE_HH_INCLUDED */
