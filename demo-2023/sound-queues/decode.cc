
#include "decode.hh"

#include <rp2040-scheduler.hh>
#include <cppos/evqueue.hh>
#include "uart.hh"

#include "opus_config.h"
#include <opus.h>

#include <chrono>
#include <thread>
#include <cstdio>
#include <bit>
#include <cstring>
#include <array>
#include <algorithm>
#include <charconv>

#define USE_OPUS 1
#include <silk/control.h>

using namespace std::literals;

// OPUS memory
extern char *global_stack;
extern char *global_stack_end;
extern char *scratch_ptr;

void dbgInt(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
extern volatile uint32_t irqCnt;
extern volatile uint32_t i2sIrqCnt;
extern volatile uint32_t i2sIrqPkt;

namespace
{
constexpr uint32_t sampleRate = 48000;

// server setting is 20ms, but the server may have some leeway
constexpr int maxFrameLenMs = 25;
constexpr int maxFrameSamples = maxFrameLenMs * (sampleRate / 1000);

typedef cppos::exec_context MyExecT;

uint32_t dbgCntDecode;

//std::array<opus_int16, maxFrameSamples * 4> decodeBuf;

std::array<char, GLOBAL_STACK_SIZE> opusStack;

// opus_decoder_get_size returned 26496
constexpr size_t opusDecoderSize = 26496;
std::array<std::byte, opusDecoderSize> decoderBuf;

WireChunk opusBuf;
PcmFrame pcmBuf;

void myAssert(bool cond)
{
    while (not cond)
    {
    }
}

constexpr int printBufSize = 180;
char printBuf[printBufSize] = "Here's the board\r\n";
inline void syncDbgOut(char const *str, size_t len)
{
#if 0
    while (!dbgOut.done())
    {
        std::this_thread::sleep_for(1ms);
        //__NOP();
    }
#endif
    dbgOut.write(str, len);
}

void dbgInfo(size_t in, size_t out)
{
    static auto bufEnd = printBuf + printBufSize;

    char *end = printBuf;
    end = std::to_chars(end, bufEnd, i2sIrqCnt).ptr;
    *(end++) = ' ';
    end = std::to_chars(end, bufEnd, i2sIrqPkt).ptr;
    *(end++) = ' ';
    end = std::to_chars(end, bufEnd, in).ptr;
    //*(end++) = ' ';
    //end = std::to_chars(end, bufEnd, out).ptr;
    *(end++) = '\r';
    *(end++) = '\n';
    syncDbgOut(printBuf, end - printBuf);
}

template <typename QT>
void dummyLoop(QT &q)
{
    static WireChunk inChunk;

    int chunkCnt = 0;
    LINT(irqCnt);
    syncDbgOut("uart started\r\n", 14);

    while (true)
    {
        q.wait_pop(inChunk);
        ++chunkCnt;
        LINT(inChunk.size);
    }
}
} // unnamed namespace

[[noreturn]] void decode()
{
    cppos::EvQueuePull<WireChunk, inBufSize, MyExecT> opusQ(inBuf);
    cppos::EvQueuePush<PcmFrame, outBufSize, MyExecT> pcmQ(outBuf);

    size_t opusSize = opus_decoder_get_size(2);
    LINT(opusSize);
    myAssert(opusSize >= opusDecoderSize);

    OpusDecoder *decoder = std::bit_cast<OpusDecoder *>(decoderBuf.data());

    global_stack = opusStack.data();
    scratch_ptr = global_stack;
    global_stack_end = global_stack + GLOBAL_STACK_SIZE;

    int err = opus_decoder_init(decoder, sampleRate, channels);
    myAssert(err == OPUS_OK);

    // we wait until the ring buffer is filled to about half
    std::this_thread::sleep_for(120ms);

    int chunkCnt = 0;
    syncDbgOut("uart started\r\n", 14);
    while (true)
    {
        opusQ.wait_pop(opusBuf);
        ++chunkCnt;

#if USE_OPUS
        auto src = reinterpret_cast<const uint8_t *>(opusBuf.payload);
        size_t len = std::min(sizeof(opusBuf.payload), opusBuf.size);
        auto dest = reinterpret_cast<int16_t *>(pcmBuf.samples);
        int samples = opus_decode(decoder,
                                  src, len,
                                  dest, maxSamplesPerFrame,
                                  0);
        myAssert(samples >= 0);

        pcmBuf.sampleCnt = samples;
        pcmQ.wait_push(pcmBuf);
#endif

        ++dbgCntDecode;
        if ((dbgCntDecode & 0x3f) == 1)
        {
            //dbgInfo(chunkCnt, 0);
            LINT(chunkCnt);
            //printf("Decoded chunk # %u\n", dbgCntDecode);
        }
    }
    dummyLoop(opusQ);
}
