#ifndef SOUND_HH_INCLUDED
#define SOUND_HH_INCLUDED

#include <cstddef>

#include <cppos/evqueue.hh>

#include "i2s.hh"  // for PcmFrame
#include "uart-recv.hh" // for WireChunk

constexpr size_t inBufSize = 40;
constexpr size_t outBufSize = 4;

extern cppos::EvQueueBody<WireChunk, inBufSize> inBuf;
extern cppos::EvQueueBody<PcmFrame, outBufSize> outBuf;

#endif // SOUND_HH_INCLUDED
