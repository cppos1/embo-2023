cmake_minimum_required(VERSION 3.24)

set(TOP_DIR ${CMAKE_CURRENT_LIST_DIR})
set(REPO_PATH /opt)
### @@@ temporary hard setting
set(OPUS_DIR /tmp/opus)

# set path to C++OS directory
file(REAL_PATH ${TOP_DIR}/../../c++os CPPOS_DIR)

# set the target board
set(BOARD rp-pico)

set(CMAKE_BUILD_TYPE Debug)

# root to search for Pico SDK and CMSIS
file(REAL_PATH /opt/CMSIS_5 CMSIS_ROOT)
# we want the modern CMSIS_5 to be searched first
file(REAL_PATH ${REPO_PATH}/pico-sdk SDK_PATH)
set(CMAKE_FIND_ROOT_PATH ${CMSIS_ROOT} ${SDK_PATH})

# the root toolchain directory (as it's not in the standard system dirs)
list(APPEND CMAKE_FIND_ROOT_PATH /opt/arm-none-eabi)

# we use the -git version
#set(COMPILER_SUFFIX -git)

# initialize C++OS build
include(${CPPOS_DIR}/cmake/use-cppos.cmake)

# our chip does have SYSTICK
set(CPPOS_TICK USE_SYSTICK)

#set(CYW43_PATH ${SDK_PATH}/lib/cyw43-driver/src)

# our project is C++ only, but OPUS is C
project(sound-queues CXX C)

#add_link_options(${TOP_DIR}/libcyw43.a)

add_compile_options(-g -finline-functions -fno-omit-frame-pointer)
add_compile_definitions(
  HAVE_CONFIG_H
  GLOBAL_STACK_SIZE=40000
)

set(TEST_SRCS
  main.cc
  decode.cc
  ../lib/uart.cc
  ../lib/timer.cc
  ../lib/rp2040.cc
)
#  snapClient.cc

add_executable(demo ${TEST_SRCS})

target_link_libraries(demo ${TOP_DIR}/../lib/libuart.a)
target_link_libraries(demo ${TOP_DIR}/../lib/libi2s.a)

add_subdirectory(../lib/opus opus)
target_link_libraries(demo opus)

# add C++OS binaries
use_cppos(demo)

# create the .lst, .bin and .hex files
cppos_add_extra_outputs(demo)

# our own project specific settings
target_include_directories(demo PUBLIC ${TOP_DIR}/../lib)
target_include_directories(demo PUBLIC ${OPUS_DIR}/include ${OPUS_DIR})
