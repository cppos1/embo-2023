// !!! this file is slide code only, it doesn't compile !!!
// multi-stage pipeline
// on core 0 we have a preemptive multi-threading scheduler w/ threadA..threadC
// we have an 5 stage pipeline that produces int numbers in the first stage,
// passes them on through the other stages and the last stage consumes
// and prints them to UART0
// task 1 (produce) as coroutine inside threadA
// task 2 (transfer2core1) as coroutine inside threadA
// task 3 (return2core0) as coroutine on core1
// task 4 (pass) as threadB (no coroutine)
// task 5 (consume) as coroutine on threadC
// on core 1 we have additional coroutines print and ledFlash

typedef cppos::exec_context MyExecT0;
typedef cppos::single_context<1> MyExecT1;

cppos::EvQueue<int, queueSize> q1;
cppos::EvQueue<int, queueSize> q2;
cppos::EvQueue<int, queueSize> q3;
cppos::EvQueue<int, queueSize> q4;

CoHandle produce()
{
    rp2040::Timer t(produceTimerNo);

    for (int i = 0; i != 40; ++i)
    {
        co_await q1.co_push(i);
        co_await t.coWaitForUs(500'000);
    }
    q1.close();
}

CoHandle transfer2core1()
{
    int i;
    while (true)
    {
        auto status = co_await q1.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            q2.close();
            break;
        }
        co_await q2.co_push(i);
    }
}

CoHandle return2core0()
{
    int i;
    while (true)
    {
        auto status = co_await q2.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            q3.close();
            break;
        }
        co_await q3.co_push(i);
    }
}

void pass()
{
    int i;
    while (true)
    {
        auto status = q3.wait_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            q4.close();
            break;
        }
        q4.wait_push(i);
    }
}

CoHandle consume()
{
    int i;
    while (true)
    {
        cppos::queue_op_status status = co_await q4.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            break;
        }
        dbgOut.write(i);
    }
}

CoHandle ledFlash()
{
    LedTimerT t;
    while (1)
    {
        co_await t.coWaitForUs(evLoop3, ledFlashRate / 2);
        led.toggle();
    }
}

CoHandle print()
{
    PrintTimerT t;

    constexpr int printBufSize = 80;
    char printBuf[printBufSize];
    auto bufEnd = printBuf + (printBufSize - 2);

    uint32_t printPeriod = 15'000'000;

    while (1)
    {
        char *end = printBuf;

        co_await t.coWaitForUs(evLoop3, printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = '\r';
        *(end++) = '\n';
        uart1.write(printBuf, end - printBuf);
    }
}

void threadA()
{
    auto coro1 = produce();
    auto coro2 = transfer2core1();

    loop();
}

void core1Proc()
{
    auto coro1 = print();
    auto coro2 = ledFlash();
    auto coro3 = return2core0();
    loop();
}

void threadB()
{
    pass();

    // never return
    while (true)
    {
        MyExecT0::yield();
    }
}

void threadC()
{
    auto coro5 = consume();
    loop();
}

int main( void )
{
    namespace exec = std::execution;

    MyExecT0 ctx0;
    MyExecT1 ctx1;

    exec::execute(ctx0->scheduler<1, 1480>(), [] { threadA(); });
    exec::execute(ctx1->scheduler<10, 1480>(), [] { core1Proc(); });
    exec::execute(ctx0->scheduler<2, 480>(), [] { threadB(); });
    exec::execute(ctx0->scheduler<3, 480>(), [] { threadC(); });

    // Start everything
    cppos::startContexts();

    /* we should never get here! */
    return 0;
}
