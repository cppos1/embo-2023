// testing event based queue with coroutines
// on core 0 we have a preemptive multi-threading scheduler w/ threadA..threadC
// on core 0 we have additional thread ledFlash
// we have an 5 stage pipeline that produces int numbers in the first stage,
// passes them on through the other stages and the last stage consumes
// and prints them to UART0
// task 1 (produce) as coroutine inside threadA
// task 2 (transfer2core1) as coroutine inside threadA
// task 3 (return2core0) as coroutine on core1
// task 4 (pass) as threadB (no coroutine)
// task 5 (consume) as coroutine on threadC
// on core 1 we have additional coroutines print and led1

#include <rp2040-scheduler.hh>

#include <uart.hh>
#include <timer.hh>
#include <pio-timer.hh>
#include "gpio.hh"
#include "helpers.hh"
#include "rp2040.hh"
#include "alloc.hh"

#include "cppos/coro.hh"
#include <cppos/evqueue.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>
#include <coroutine>
#include <atomic>

#include <cppos/scheduler.tt>


using namespace std::literals;


#define LINT(x) dbgIntBlock(__LINE__, x)
#define COLINT(w,x) \
    do \
    { \
        LineValBuf buf; \
        size_t len = dbgInt(buf, __LINE__, x); \
        while (not syncDbgOut(buf.data(), len, true))  \
        { \
            co_await w.first->coWaitForUs(*(w.second), 10'000); \
        } \
    } while (false)

#define LINT1(x) uart1Int(__LINE__, x)
void uart1Char(char val)
{
    uart1.write(&val, 1);
}
void uart1Int(int line, unsigned val);
void uart1IntSync(int line, unsigned val);
uint32_t dbgTEv[4];

namespace
{
using cppos::CoHandle;

constexpr uint32_t queueSize = 8;

constexpr uint32_t led0Pin = 18;
constexpr uint32_t led1Pin = 19;
constexpr uint32_t led2Pin = 20;
constexpr uint32_t led3Pin = 21;
constexpr uint32_t led5Pin = 22;
constexpr uint32_t led4Pin = 26;
constexpr uint32_t led6Pin = 27;
constexpr uint32_t led7Pin = 28;

constexpr auto ledPort = 0;
constexpr uint32_t ledPin = 25;
constexpr auto ledFlashRate = 800'000;
constexpr auto led7FlashRate = 800ms;
GpioOut<ledPort, ledPin> led(true);
GpioOut<ledPort, led7Pin> led7(true);

constexpr uint32_t dbgTimerNo = 0;
constexpr uint32_t produceTimerNo = 1;
constexpr uint32_t passTimerNo = 2;
constexpr uint32_t consumeTimerNo = 3;
typedef rp2040::PioTimer<0, 0> PrintTimerT;
typedef rp2040::PioTimer<0, 1> LedTimerT;
bool uart1Disable = false;

//constexpr int printBuf2Size = 80;
//char printBuf2[printBuf2Size] = "Here's the board\r\n";
constexpr size_t maxLineDigits = 5;
constexpr size_t maxValDigits = 9;
constexpr size_t lineValBufSize = maxLineDigits + 1 + maxValDigits + 2;
typedef std::array<char, lineValBufSize> LineValBuf;

constexpr unsigned coroBufSize = 1000;
MemBuf<coroBufSize> coroBuf;


typedef cppos::exec_context MyExecT0;
typedef cppos::single_context<1> MyExecT1;

alignas(4) uint8_t execCtx0Buf[sizeof(MyExecT0) + 40];
alignas(4) uint8_t execCtx1Buf[sizeof(MyExecT1) + 40];

typedef cppos::CoLoop<MyExecT0> MyLoopT0;
typedef cppos::CoLoop<MyExecT1> MyLoopT1;

MyLoopT0 evLoop12;
MyLoopT1 evLoop3;
MyLoopT0 evLoop5;

cppos::EvQueueBody<int, queueSize> q1;
cppos::EvQueueCoPush<int, queueSize, MyLoopT0> pushQ1(q1, evLoop12);
cppos::EvQueueCoPull<int, queueSize, MyLoopT0> pullQ1(q1, evLoop12);

cppos::EvQueueBody<int, queueSize> q2;
cppos::EvQueueCoPush<int, queueSize, MyLoopT0> pushQ2(q2, evLoop12);
cppos::EvQueueCoPull<int, queueSize, MyLoopT1> pullQ2(q2, evLoop3);
//cppos::EvQueueCoPull<int, queueSize, MyLoopT0> pullQ2(q2, evLoop12);

cppos::EvQueueBody<int, queueSize> q3;
cppos::EvQueueCoPush<int, queueSize, MyLoopT1> pushQ3(q3, evLoop3);
//cppos::EvQueueCoPush<int, queueSize, MyLoopT0> pushQ3(q3, evLoop12);
cppos::EvQueuePull<int, queueSize, MyExecT0> pullQ3(q3);

cppos::EvQueueBody<int, queueSize> q4;
cppos::EvQueuePush<int, queueSize, MyExecT0> pushQ4(q4);
cppos::EvQueueCoPull<int, queueSize, MyLoopT0> pullQ4(q4, evLoop5);

std::atomic<bool> dbgOutBusy = false;
std::atomic<unsigned> dbgOuterCnt = 0;
std::atomic<unsigned> dbgOuterCntMax = 0;
std::atomic<unsigned> dbgInnerCnt = 0;
std::atomic<unsigned> dbgInnerCntMax = 0;
std::atomic<unsigned> dbgCnt = 0;

typedef std::pair<rp2040::Timer *, MyLoopT0 *> CoWaiter;
CoWaiter wait12{nullptr, &evLoop12};
CoWaiter wait5{nullptr, &evLoop5};

#if 0
//CoHandle syncDbgOut(char const *str, size_t len)
void syncDbgOut(char const *str, size_t len)
{
    //rp2040::Timer t(dbgTimerNo);
    while (!dbgOut.done())
    {
        //co_await t.coWaitForUs(evLoop12, 1'000);
        __NOP();
    }
    dbgOut.write(str, len);
}
#else
bool syncDbgOut(char const *str, size_t len, bool dontBlock = false)
{
    dbgCnt += len;
    ++dbgOuterCnt;
    if (dbgOuterCnt > dbgOuterCntMax)
    {
        dbgOuterCntMax = dbgOuterCnt.load();
    }
    bool lock = false;
    while (not dbgOutBusy.compare_exchange_strong(lock, true))
    {
        if (dontBlock)
        {
            --dbgOuterCnt;
            return false;
        }
        MyExecT0::yield();
        lock = false;
    }

    ++dbgInnerCnt;
    if (dbgInnerCnt > dbgInnerCntMax)
    {
        dbgInnerCntMax = dbgInnerCnt.load();
    }

    char const *s = str;
    while (true)
    {
        size_t written = dbgOut.write(str, len);
        if (written == len)
        {
            break;
        }

        len -= written;
        s += written;
    }
    --dbgInnerCnt;

    dbgOutBusy = false;
    --dbgOuterCnt;

    return true;
}
#endif

size_t dbgInt(LineValBuf &buf, int line, unsigned val)
{
    auto bufEnd = buf.data() + (buf.size() - 2);
    char *end = buf.data();

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';

    return end - buf.data();
}

void dbgIntBlock(int line, unsigned val)
{
    LineValBuf buf;
    size_t len = dbgInt(buf, line, val);

    syncDbgOut(buf.data(), len);
}

inline void system_setup()
{
    cppos::port::disable_interrupts();

    // on reset all spinlocks are locked
    // unlock them
    constexpr static Reg slReg{SIO_SPINLOCK0};
    for (int i = 0; i != 32; ++i)
    {
        slReg.ptr()[i] = 1;
    }
}

CoHandle produce()
{
    syncDbgOut("ProdStart\r\n", 11);
    rp2040::Timer t(produceTimerNo);
    wait12.first = &t;

    for (int i = 0; i != 40; ++i)
    {
        COLINT(wait12, i);
        co_await pushQ1.co_push(i);
        COLINT(wait12, i);
        // for some reason this is way too slow
        // (more than 1s!)
        co_await t.coWaitForUs(evLoop12, 500'000);
    }
    pushQ1.close();
}

CoHandle transfer2core1()
{
    syncDbgOut("Hello t2c1\r\n", 12);

    int i = 0;
    while (true)
    {
        auto status = co_await pullQ1.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            pushQ2.close();
            break;
        }
        COLINT(wait12, i);

        co_await pushQ2.co_push(i);
    }
    COLINT(wait12, i);
}

CoHandle return2core0()
{
    uart1.write("Hello r2c0\r\n", 12);

    int i = 0;
    while (true)
    {
        auto status = co_await pullQ2.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            pushQ3.close();
            break;
        }
        LINT1(i);

        co_await pushQ3.co_push(i);
    }
    LINT1(i);
}

void pass()
{
    syncDbgOut("Hello pass\r\n", 12);
    rp2040::Timer t(passTimerNo);
    t.waitForUs<MyExecT0>(10'000'000);
    syncDbgOut("PassStart\r\n", 11);

    int i = 0;
    while (true)
    {
        auto status = pullQ3.wait_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            pushQ4.close();
            break;
        }
        LINT(i);

        pushQ4.wait_push(i);
    }
    LINT(i);
}

CoHandle consume()
{
    rp2040::Timer t(consumeTimerNo);
    wait5.first = &t;
    syncDbgOut("ConsumeStart\r\n", 14);

    int i = 0;
    while (true)
    {
        cppos::queue_op_status status = co_await pullQ4.co_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            COLINT(wait5, unsigned(status));
            break;
        }

        COLINT(wait5, i);
    }
    COLINT(wait5, i);
}

CoHandle ledFlash()
{
    LedTimerT t;
    while (1)
    {
        co_await t.coWaitForUs(evLoop3, ledFlashRate / 2);
        led.toggle();
    }
}

CoHandle print()
{
    PrintTimerT t;

    constexpr int printBufSize = 80;
    char printBuf[printBufSize];
    auto bufEnd = printBuf + (printBufSize - 2);

    uint32_t printPeriod = 15'000'000;

    while (1)
    {
        char *end = printBuf;

        co_await t.coWaitForUs(evLoop3, printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, dbgOuterCntMax).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, dbgInnerCntMax).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, dbgCnt).ptr;
        *(end++) = '\r';
        *(end++) = '\n';
        uart1.write(printBuf, end - printBuf);
    }
}

void threadA()
{
    syncDbgOut("T1", 2);
    auto coro1 = produce();

    syncDbgOut("T2", 2);
    auto coro2 = transfer2core1();

    syncDbgOut("L12", 3);
    evLoop12.start();
}

void core1Proc()
{
    uart1.write("Here's the board\r\n", 18);
    uart1.write("TP", 2);
    auto coro1 = print();
    uart1.write("TL", 2);
    auto coro2 = ledFlash();
    uart1.write("T3", 2);
    auto coro3 = return2core0();

    uart1.write("L3", 2);
    evLoop3.start();
}

void threadB()
{
    syncDbgOut("T4", 2);
    pass();

    // never return
    while (true)
    {
        MyExecT0::yield();
    }
}

void threadC()
{
    syncDbgOut("T5", 2);
    auto coro5 = consume();
    syncDbgOut("L5", 2);

    evLoop5.start();
}

void led7Task()
{
    while (1)
    {
        std::this_thread::sleep_for(led7FlashRate/2);
        led7.toggle();
    }
}

MyExecT0 *ctx0 = nullptr;
MyExecT1 *ctx1 = nullptr;
} // unnamed namespace

// global helpers
void uart1Int(int line, unsigned val)
{
    char printBuf2[printBuf2Size];

    if (uart1Disable) return;
    auto bufEnd = printBuf2 + printBuf2Size;
    char *end = printBuf2;

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';

#if 0
    while (!uart1.done())
    {
        //std::this_thread::sleep_for(1ms);
        __NOP();
    }
#endif
    uart1.write(printBuf2, end - printBuf2);
}

void uart1IntSync(int line, unsigned val)
{
    uart1Disable = true;
    while (!uart1.done()) ;
    uart1Disable = false;
    uart1Int(line, val);
    uart1Disable = true;
    while (!uart1.done()) ;
    uart1Disable = false;
}

namespace cppos
{
void *DummyPromise::operator new(size_t size) noexcept
{
    void *p = coroBuf.alloc(size);
    while (!p)
    {
        error_handler();
    }
    return p;
}

void DummyPromise::operator delete(void *) noexcept
{
}
} // namespace cppos

extern "C"
{
void SystemInit()
{
    rp2040::clocks_init();

    enablePeripheral(RESETS_RESET_io_bank0_Msk | RESETS_RESET_pads_bank0_Msk);
    enablePeripheral(RESETS_RESET_timer_Msk);
    enablePeripheral(RESETS_RESET_pio0_Msk | RESETS_RESET_pio1_Msk);

    /*
     * Missing:
     *
     * - ROM functions
     *   - possibly use pico_mem_ops and pico_bit_ops from SDK
     */
}

void error_handler()
{
    cppos::port::disable_interrupts();
    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    dbgOut.write("Here's the board\r\n", 18);

    namespace exec = std::execution;

    ctx0 = new (execCtx0Buf) MyExecT0;
    ctx1 = new (execCtx1Buf) MyExecT1;

    exec::execute(ctx0->scheduler<1, 1480>(), [] { threadA(); });
    exec::execute(ctx1->scheduler<10, 1480>(), [] { core1Proc(); });
    exec::execute(ctx0->scheduler<2, 480>(), [] { threadB(); });
    exec::execute(ctx0->scheduler<3, 1480>(), [] { threadC(); });
    exec::execute(ctx0->scheduler<5, 440>(), [] { led7Task(); });
/*
    exec::execute(ctx0->scheduler<1, 440>(), [] { ledFlashTask(); });
    exec::execute(ctx0->scheduler<2, 880>(), [] { printTask(); });
    exec::execute(ctx0->scheduler<10, 800>(), [] { task1(); });
    exec::execute(ctx1->scheduler<11, 800>(), [] { task2(); });
    exec::execute(ctx0->scheduler<12, 800>(), [] { task3(); });
*/
    // Start everything
    cppos::startContexts();

    /* we should never get here! */
    return 0;
}
