// Task to blink an LED


#include "gpio.hh"

#include <chrono>
#include <thread>

#include <stdint.h>


using namespace std::literals;

namespace
{
constexpr auto ledPort = 0;
constexpr uint32_t ledPin = 25;

constexpr auto ledFlashRate = 800ms;

GpioOut<ledPort, ledPin> led(true);
} // unnamed namespace

void ledFlashTask()
{
    while (1)
    {
        std::this_thread::sleep_for(ledFlashRate/2);
        led.toggle();
    }
}
