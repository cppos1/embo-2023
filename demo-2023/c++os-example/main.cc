// very simple scheduling example

#include <cppos/scheduler.hh>

#include <uart.hh>
#include "helpers.hh"

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>
#include <atomic>

#include <cppos/scheduler.tt>
#include <port-header.hh>

#include <rp2040.h>

#define SHIFT_VALUE(x,v) (x##_##v << x##_Pos)

using namespace std::literals;


void ledFlashTask();
void busyTask(void *);

std::atomic<uint32_t> *cntPtr[3] = {nullptr};
std::atomic<uint32_t> counters[3] = {0};

namespace
{
constexpr uint32_t sysFreq = 16'000'000;
constexpr uint32_t sysTickFreq = 1000;  // we set systick to 1ms

uint8_t execCtxBuf[sizeof(cppos::exec_context)];

char buf[80] = "Here's the board\r\n";

inline void system_setup()
{
    cppos::port::disable_interrupts();
}

void clocks_init()
{
    // it looks like writing to the bitfields is generally not a good idea
    // it causes a read-modify-write while often we want a write only
    // reading is fine
    // XOSC procedure from datasheet
    XOSC->CTRL = SHIFT_VALUE(XOSC_CTRL_FREQ_RANGE, 1_15MHZ);
    XOSC->STARTUP = 47;
    XOSC_SET->CTRL = SHIFT_VALUE(XOSC_CTRL_ENABLE, ENABLE);
    while (XOSC->STATUS_b.STABLE == 0)
        ;

    CLOCKS->CLK_REF_CTRL = SHIFT_VALUE(CLOCKS_CLK_REF_CTRL_SRC, xosc_clksrc);
    while (CLOCKS->CLK_REF_SELECTED != (1 << CLOCKS_CLK_REF_CTRL_SRC_xosc_clksrc))
        ;
    CLOCKS->CLK_SYS_CTRL = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_SRC, clk_ref);
    while (CLOCKS->CLK_SYS_SELECTED != (1 << CLOCKS_CLK_SYS_CTRL_SRC_clk_ref))
        ;


    // SYS PLL 126MHz from datasheet
    enablePeripheral(RESETS_RESET_pll_sys_Msk);
    PLL_SYS->CS = 1 << PLL_SYS_CS_REFDIV_Pos;
    PLL_SYS->FBDIV_INT = 63;
    PLL_SYS_CLR->PWR = PLL_SYS_PWR_PD_Msk | PLL_SYS_PWR_VCOPD_Msk;
    while (PLL_SYS->CS_b.LOCK == 0)
        ;
    PLL_SYS->PRIM = (6 << PLL_SYS_PRIM_POSTDIV1_Pos) |
        1 << (PLL_SYS_PRIM_POSTDIV2_Pos);
    PLL_SYS_CLR->PWR = 1 << PLL_SYS_PWR_POSTDIVPD_Pos;

    CLOCKS->CLK_SYS_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_AUXSRC, clksrc_pll_sys);
    CLOCKS->CLK_SYS_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_SYS_CTRL_SRC, clksrc_clk_sys_aux);
    while (CLOCKS->CLK_SYS_SELECTED !=
           (1 << CLOCKS_CLK_SYS_CTRL_SRC_clksrc_clk_sys_aux))
        ;

    // no need to stop, we're coming out of reset
    CLOCKS->CLK_PERI_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_PERI_CTRL_AUXSRC, clk_sys);
    CLOCKS_SET->CLK_PERI_CTRL = 1 << CLOCKS_CLK_PERI_CTRL_ENABLE_Pos;


    // we don't enable the watchdog, but we must start it
    // for the reference timer
    WATCHDOG_SET->TICK = (WATCHDOG_TICK_ENABLE_Msk |
                          (12 << WATCHDOG_TICK_CYCLES_Pos));


#if 0
    // USB PLL from datasheet
    enablePeripheral(RESETS_RESET_pll_usb_Msk);
    PLL_USB->CS = 1 << PLL_SYS_CS_REFDIV_Pos;
    PLL_USB->FBDIV_INT = 64;
    PLL_USB_CLR->PWR = PLL_SYS_PWR_PD_Msk | PLL_SYS_PWR_VCOPD_Msk;
    while (PLL_USB->CS_b.LOCK == 0)
        ;
    PLL_USB->PRIM = (4 << PLL_SYS_PRIM_POSTDIV1_Pos) |
        4 << (PLL_SYS_PRIM_POSTDIV2_Pos);
    PLL_USB_CLR->PWR = 1 << PLL_SYS_PWR_POSTDIVPD_Pos;

    // no need to stop, we're coming out of reset
    CLOCKS->CLK_USB_CTRL
        = SHIFT_VALUE(CLOCKS_CLK_USB_CTRL_AUXSRC, clksrc_pll_usb);
    CLOCKS_SET->CLK_USB_CTRL = 1 << CLOCKS_CLK_USB_CTRL_ENABLE_Pos;
#endif
}

uint32_t rp2040Ticks_us()
{
    return TIMER->TIMERAWL;
}

void printTask()
{
    dbgOut.write(buf, 18);
    auto bufEnd = buf + 80;

    auto printPeriod = 10s;

    while (1)
    {
        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        char *end = buf;
        end = std::to_chars(end, bufEnd, rp2040Ticks_us()).ptr;
        *(end++) = ' ';
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = ' ';
        for (int i = 0; i != 3; ++i)
        {
            end = std::to_chars(end, bufEnd, counters[i]).ptr;
            *(end++) = ' ';
            uint32_t localCnt = *(cntPtr[i]);
            end = std::to_chars(end, bufEnd, localCnt).ptr;
            *(end++) = ' ';
        }
        *(end++) = '\r';
        *(end++) = '\n';
        dbgOut.write(buf, end - buf);
    }
}

struct task_info
{
    void volatile *used_stack;
    uint8_t idx = 255; // index in array
};

std::array<task_info, 6> tasks;
cppos::exec_context *ctx = nullptr;
} // unnamed namespace

namespace cppos
{
void volatile *debug_handle()
{
    std::byte volatile *p;

    for (size_t i = 0; i != 6; ++i)
    {
        tasks[i].idx = i;
        if (i != 5)
        {
            p = static_cast<std::byte volatile *>(ctx->tasks[i+1].stack_end);
        }
        else
        {
            p = static_cast<std::byte volatile *>(ctx->assigned_stack_top);
        }
        p += sys_traits::default_stack_guard_size;
        for ( ; p != ctx->tasks[i].stack_end; ++p)
        {
            if (*p != std::byte(0xf0))
            {
                break;
            }
        }
        tasks[i].used_stack = p;
    }

    p += 10; // dummy for breakpoint

    return p;
}
} // namespace cppos

extern "C"
{
void SystemInit()
{
    clocks_init();

    // we enable the pins here, as we're going to need them anyways
    enablePeripheral(RESETS_RESET_io_bank0_Msk | RESETS_RESET_pads_bank0_Msk);

    /*
     * Missing:
     *
     * - ROM functions
     *   - possibly use pico_mem_ops and pico_bit_ops from SDK
     * - spinlocks
     */
}

void error_handler()
{
    cppos::debug_handle();

    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    namespace exec = std::execution;
    ctx = new (execCtxBuf) cppos::exec_context;

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<1, 440>(ctx), [] { ledFlashTask(); });
    exec::execute(cppos::scheduler<2, 480>(ctx), [] { printTask(); });
    exec::execute(cppos::scheduler<10>(ctx), [] { busyTask(counters); });
    exec::execute(cppos::scheduler<11>(ctx), [] { busyTask(counters + 1); });
    exec::execute(cppos::scheduler<12>(ctx), [] { busyTask(counters + 2); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
