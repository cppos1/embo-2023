
#include <cstdint>
#include <atomic>

extern std::atomic<uint32_t> *cntPtr[3];
extern std::atomic<uint32_t> counters[3];

void busyTask(void *taskVar)
{
    std::atomic<uint32_t> count = 0;
    std::atomic<uint32_t> *taskCnt
        = static_cast<std::atomic<uint32_t> *>(taskVar);
    uint8_t id = taskCnt - counters;
    cntPtr[id] = &count;

    while (true)
    {
        ++count;
        if (count == 1'000'000)
        {
            count = 0;
            ++(*taskCnt);
        }
    }
}
