// testing event based queue

#include <rp2040-scheduler.hh>

#include <uart.hh>
#include "gpio.hh"
#include "helpers.hh"
#include "rp2040.hh"

#include <cppos/evqueue.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>
//#include <atomic>

#include <cppos/scheduler.tt>


using namespace std::literals;


#define LINT(x) dbgInt(__LINE__, x)
namespace
{
constexpr uint32_t queueSize = 16;

constexpr auto ledPort = 0;
constexpr uint32_t ledPin = 25;
constexpr auto ledFlashRate = 800ms;
GpioOut<ledPort, ledPin> led(true);

constexpr int printBufSize = 180;
char printBuf[printBufSize] = "Here's the board\r\n";
constexpr int printBuf2Size = 60;
char printBuf2[printBuf2Size] = "Here's the board\r\n";


typedef cppos::exec_context MyExecT;

cppos::EvQueueBody<int, queueSize> q1;
cppos::EvQueuePush<int, queueSize, MyExecT> pushQ1(q1);
cppos::EvQueuePull<int, queueSize, MyExecT> pullQ1(q1);

cppos::EvQueueBody<int, queueSize> q2;
cppos::EvQueuePush<int, queueSize, MyExecT> pushQ2(q2);
cppos::EvQueuePull<int, queueSize, MyExecT> pullQ2(q2);

alignas(4) uint8_t execCtxBuf[sizeof(MyExecT) + 40];

inline void syncDbgOut(char const *str, size_t len)
{
    while (!dbgOut.done())
    {
        std::this_thread::sleep_for(1ms);
        //__NOP();
    }
    dbgOut.write(str, len);
}

void dbgInt(int line, unsigned val)
{
    auto bufEnd = printBuf + printBufSize;
    char *end = printBuf;

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';
    syncDbgOut(printBuf, end - printBuf);
}

inline void system_setup()
{
    cppos::port::disable_interrupts();

    // on reset all spinlocks are locked
    // unlock them
    constexpr static Reg slReg{SIO_SPINLOCK0};
    for (int i = 0; i != 32; ++i)
    {
        slReg.ptr()[i] = 1;
    }
}

void task1()
{
    syncDbgOut("T1Start\r\n", 9);

    for (int i = 0; i != 100; ++i)
    {
        pushQ1.wait_push(i);
        LINT(i);
        std::this_thread::sleep_for(500ms);
    }
    pushQ1.close();

    while (true)
    {
        std::this_thread::sleep_for(100s);
    }
}

void task2()
{
    syncDbgOut("T2Start\r\n", 9);
    std::this_thread::sleep_for(10s);
    syncDbgOut("T2Start\r\n", 9);

    int i = 0;
    while (true)
    {
        auto status = pullQ1.wait_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            pushQ2.close();
            break;
        }
        LINT(i);

        pushQ2.wait_push(i);
        //debug();
    }
    LINT(i);

    while (true)
    {
        std::this_thread::sleep_for(100s);
    }
}

void task3()
{
    syncDbgOut("T3Start\r\n", 9);

    int i = 0;
    while (true)
    {
        auto status = pullQ2.try_pop(i);
        if (status == cppos::queue_op_status::closed)
        {
            LINT(unsigned(status));
            break;
        }

        if (status == cppos::queue_op_status::empty)
        {
            //LINT(unsigned(status));
            //debug();
            std::this_thread::sleep_for(100ms);
            continue;
        }
        LINT(i);
    }
    LINT(i);

    while (true)
    {
        std::this_thread::sleep_for(100s);
    }
}

void ledFlashTask()
{
    while (1)
    {
        std::this_thread::sleep_for(ledFlashRate/2);
        led.toggle();
    }
}

void printTask()
{
    uart1.write(printBuf2, 18);
    auto bufEnd = printBuf2 + printBuf2Size;

    auto printPeriod = 2s;

    while (1)
    {
        char *end = printBuf2;

        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = '\r';
        *(end++) = '\n';
        uart1.write(printBuf2, end - printBuf2);
    }
}

MyExecT *ctx = nullptr;
} // unnamed namespace


extern "C"
{
void SystemInit()
{
    rp2040::clocks_init();

    // we enable the pins here, as we're going to need them anyways
    enablePeripheral(RESETS_RESET_io_bank0_Msk | RESETS_RESET_pads_bank0_Msk);

    /*
     * Missing:
     *
     * - ROM functions
     *   - possibly use pico_mem_ops and pico_bit_ops from SDK
     */
}

void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"

void debug()
{
    LINT(q1.cnt);
    LINT(q2.cnt);
    LINT(pullQ2.body.cnt);
}

int main( void )
{
    system_setup();
    dbgOut.write(printBuf, 18);

    namespace exec = std::execution;
    ctx = new (execCtxBuf) cppos::exec_context;

    // we have a bug if the task is a function pointer
    exec::execute(ctx->scheduler<1, 440>(), [] { ledFlashTask(); });
    exec::execute(ctx->scheduler<2, 880>(), [] { printTask(); });
    exec::execute(ctx->scheduler<10, 800>(), [] { task1(); });
    exec::execute(ctx->scheduler<11, 800>(), [] { task2(); });
    exec::execute(ctx->scheduler<12, 800>(), [] { task3(); });

    // Start everything
    cppos::startContexts();

    /* we should never get here! */
    return 0;
}
