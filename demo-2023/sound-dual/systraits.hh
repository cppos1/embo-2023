#ifndef SYSTRAITS_HH_INCLUDED
#define SYSTRAITS_HH_INCLUDED

#include <cstdint>

void timerConfig();

namespace cppos
{
struct sys_traits
{
    static constexpr int static_task_count = 5;
    static constexpr int dynamic_task_count = 2;
    static constexpr int default_stack_size = 400;
    static constexpr int default_stack_guard_size = 4;
    static constexpr uint32_t default_stack_guard_value = 0xdead66aa;
    static constexpr int default_priority = 10;
    static constexpr int max_function_size = 8;
    static constexpr int min_stack_size = 160;
    static constexpr int main_stack_size = 400;
    static constexpr int max_loop_tasks = 5;
    static constexpr uint8_t max_event_count = 16;
    //static constexpr uint32_t stack_start = 0x;
    //static constexpr uint32_t stack_end = 0x;
};

struct port_traits
{
    // the scheduler doesn't need to know about the time slice
    // it's the port that need to handle this
    static constexpr int time_slice_ms = 1;
    static constexpr uint32_t sys_timer_value = 126'000;
    // for now we need this :-(
    static constexpr void(*sys_timer_setup)() = nullptr;
    // the SDK should define which one to use
    // I've seen an ADA implementation that uses 31, but I think
    // it should be one of the reserved ones
    static constexpr uint32_t atomic_spinlock_no = 5;
    static constexpr uint32_t used_cores = 2;
};

struct board_traits
{
    static constexpr bool useUart1 = false;
};
} // namespace cppos
#endif /* SYSTRAITS_HH_INCLUDED */
