
#include "decode.hh"

#include <rp2040-scheduler.hh>
#include <cppos/evqueue.hh>
#include "uart.hh"
#include "timer.hh"

#include "opus_config.h"
#include <opus.h>

#include <chrono>
#include <thread>
#include <cstdio>
#include <bit>
#include <cstring>
#include <array>
#include <algorithm>
#include <charconv>

#define USE_OPUS 1
#include <silk/control.h>

using namespace std::literals;

// OPUS memory
extern char *global_stack;
extern char *global_stack_end;
extern char *scratch_ptr;

void dbgInt(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)

namespace
{
constexpr uint32_t sampleRate = 48000;

// server setting is 20ms, but the server may have some leeway
constexpr int maxFrameLenMs = 25;
constexpr int maxFrameSamples = maxFrameLenMs * (sampleRate / 1000);

// this runs on core1
typedef cppos::single_context<1> MyExecT;

uint32_t dbgCntDecode;

//std::array<opus_int16, maxFrameSamples * 4> decodeBuf;

std::array<char, GLOBAL_STACK_SIZE> opusStack;

// opus_decoder_get_size returned 26496
constexpr size_t opusDecoderSize = 26496;
std::array<std::byte, opusDecoderSize> decoderBuf;

WireChunk opusBuf;
PcmFrame pcmBuf;

void myAssert(bool cond)
{
    while (not cond)
    {
    }
}
} // unnamed namespace

[[noreturn]] void decode()
{
    cppos::EvQueuePull<WireChunk, inBufSize, MyExecT> opusQ(inBuf);
    cppos::EvQueuePush<PcmFrame, outBufSize, MyExecT> pcmQ(outBuf);
    dbgOut.write("decoder started\r\n", 17);

    // we need this inside the function to get the interrupt
    // on our core
    rp2040::Timer iniWait(1);

    size_t opusSize = opus_decoder_get_size(2);
    LINT(opusSize);
    myAssert(opusSize >= opusDecoderSize);

    OpusDecoder *decoder = std::bit_cast<OpusDecoder *>(decoderBuf.data());

    global_stack = opusStack.data();
    scratch_ptr = global_stack;
    global_stack_end = global_stack + GLOBAL_STACK_SIZE;

    int err = opus_decoder_init(decoder, sampleRate, channels);
    myAssert(err == OPUS_OK);

    // we wait until the ring buffer is filled to about half
    opusQ.wait_pop(opusBuf); // we skip the first packet
    iniWait.waitForUs<MyExecT>((inBufSize / 2) * 20'000);
    dbgOut.write("Starting decode...\r\n", 20);

    int chunkCnt = 0;
    while (true)
    {
        opusQ.wait_pop(opusBuf);
        ++chunkCnt;

#if USE_OPUS
        auto src = reinterpret_cast<const uint8_t *>(opusBuf.payload);
        size_t len = std::min(sizeof(opusBuf.payload), opusBuf.size);
        auto dest = reinterpret_cast<int16_t *>(pcmBuf.samples);
        int samples = opus_decode(decoder,
                                  src, len,
                                  dest, maxSamplesPerFrame,
                                  0);

        myAssert(samples >= 0);

        pcmBuf.sampleCnt = samples;
        pcmQ.wait_push(pcmBuf);
#endif

        ++dbgCntDecode;
        if ((dbgCntDecode & 0x7f) == 1)
        {
            LINT(chunkCnt);
        }
    }
}
