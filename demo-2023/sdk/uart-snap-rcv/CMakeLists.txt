cmake_minimum_required(VERSION 3.19)

set(TOP_DIR ${CMAKE_CURRENT_LIST_DIR})

set(CMAKE_BUILD_TYPE Debug)

set(PICO_BOARD "pico_w")
set(BOARD "pico-w")
set(PICO_BARE_METAL true)

set(PICO_SDK_PATH /opt/pico-sdk)

#file(REAL_PATH ${TOP_DIR}/../../../c++os CPPOS_DIR)

include(${PICO_SDK_PATH}/pico_sdk_init.cmake)

project(uart-lib)

pico_sdk_init()

add_compile_options(-g -finline-functions -fno-omit-frame-pointer)

include_directories(${TOP_DIR})
include_directories(../dummy)


add_library(uart receiver.cc ../minimal-sdk-helpers.cc)

target_include_directories(uart PRIVATE
  ${PICO_SDK_PATH}/src/common/pico_base/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_base/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_gpio/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_irq/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_dma/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_pio/include
  ${PICO_SDK_PATH}/src/rp2_common/hardware_uart/include
  ${PICO_SDK_PATH}/src/rp2_common/pico_platform/include
  ${PICO_SDK_PATH}/src/rp2040/hardware_regs/include
  ${PICO_SDK_PATH}/src/rp2040/hardware_structs/include
)
#  ${CPPOS_DIR}/include/std
#  ${CPPOS_DIR}/include/cppos/ports/rp2040
#  ${CPPOS_DIR}/support/rp-pico
