
#include "uart-recv.hh"
#include "hardware/gpio.h"
#include "hardware/dma.h"
#include "hardware/irq.h"
#include "hardware/uart.h"
#include <algorithm>


void dbgInt(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
extern volatile uint32_t irqCnt;
extern volatile uint32_t lastSize;

extern "C"
{
#if BUILD_WITH_SDK
static void __isr uartRecvDmaIrqHandler();
#endif
} // extern "C"


namespace
{
constexpr uint32_t uartDmaChannel = 1;
constexpr uint32_t uartDmaIrq = 1;

struct [[gnu::packed]] SnapHeader
{
    uint16_t type;
    uint8_t fields[20];
    size_t payloadSize;
};

struct [[gnu::packed]] AlignedHeader
{
    uint16_t padding;
    SnapHeader hdr;
};

AlignedHeader hdrBuf;
WireChunk chunkBuf;
bool receivingHdr;
} // unnamed namespace


void uartDmaSetup()
{
    dma_channel_config dmaCfg = dma_channel_get_default_config(uartDmaChannel);

    channel_config_set_dreq(&dmaCfg, DREQ_UART1_RX);
    channel_config_set_transfer_data_size(&dmaCfg, DMA_SIZE_8);
    channel_config_set_read_increment(&dmaCfg, false);
    channel_config_set_write_increment(&dmaCfg, true);
    dma_channel_configure(uartDmaChannel,
                          &dmaCfg,
                          NULL,  // dest
                          &uart1_hw->dr, // src
                          0, // count
                          false); // trigger

#if BUILD_WITH_SDK
    irq_set_exclusive_handler(DMA_IRQ_1, uartRecvDmaIrqHandler);
#endif
    dma_irqn_set_channel_enabled(uartDmaIrq, uartDmaChannel, 1);
}

void uartStart()
{
    irq_set_enabled(DMA_IRQ_1, true);

    receivingHdr = true;
    dma_channel_set_write_addr(uartDmaChannel, &(hdrBuf.hdr), false);
    dma_channel_set_trans_count(uartDmaChannel, sizeof(SnapHeader), true);
}

extern "C"
{
#if BUILD_WITH_SDK
void __isr uartRecvDmaIrqHandler()
#else
void DMA1_IRQHandler()
#endif
{
    if (!dma_irqn_get_channel_status(uartDmaIrq, uartDmaChannel))
    {
        return;
    }
    dma_irqn_acknowledge_channel(uartDmaIrq, uartDmaChannel);
    ++irqCnt;

    if (receivingHdr)
    {
        lastSize = hdrBuf.hdr.payloadSize;
        dma_channel_set_write_addr(uartDmaChannel, &chunkBuf, false);
        uint32_t len = std::min(sizeof(chunkBuf), hdrBuf.hdr.payloadSize);
        dma_channel_set_trans_count(uartDmaChannel, len, true);
        receivingHdr = false;
    }
    else
    {
        if (hdrBuf.hdr.type == 2)
        {
            pushChunk(chunkBuf);
        }
        dma_channel_set_write_addr(uartDmaChannel, &(hdrBuf.hdr), false);
        dma_channel_set_trans_count(uartDmaChannel, sizeof(SnapHeader), true);
        receivingHdr = true;
    }
}
} // extern "C"
