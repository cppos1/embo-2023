#ifndef UART_RECV_HH_INCLUDED
#define UART_RECV_HH_INCLUDED

#include <cstddef>
#include <cstdint>

// for 32kbps and one frame each 20ms I've seen up to 120 bytes
constexpr size_t maxOpusFrameSize = 150;
//constexpr size_t maxBytesPerFrame = 1024;
constexpr size_t maxBytesPerFrame = maxOpusFrameSize;

struct [[gnu::packed]] WireChunk
{
    uint32_t stamp[2];
    size_t size;
    uint8_t payload[maxBytesPerFrame];
};

void uartDmaSetup();
void uartStart();

void pushChunk(WireChunk const &);

#endif /* UART_RECV_HH_INCLUDED */
