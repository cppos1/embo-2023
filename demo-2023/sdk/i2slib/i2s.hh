#ifndef I2S_HH_INCLUDED
#define I2S_HH_INCLUDED

#include <cstdint>
#include <cstddef>


// 20ms at 44'100 gives 882 samples
// 20ms at 48'000 gives 960 samples
constexpr size_t maxSamplesPerFrame = 1'024;

constexpr size_t pcmBufferSize = 8;

// currently for I2S we only support stereo in, stereo out, 16 bit
struct AudioConfig
{
    uint32_t sampleFreqHz;
};

struct AudioI2sConfig
{
    uint8_t dataOutPin;
    uint8_t bClkPin;
};

struct PcmFrame
{
    size_t sampleCnt;
    uint32_t samples[maxSamplesPerFrame];
};

void i2sSetup(AudioI2sConfig const *cfg, AudioConfig const *fmt);
void i2sStart();

// to be provided by main program
bool pullPcmFrame(PcmFrame &);

#endif /* I2S_HH_INCLUDED */
