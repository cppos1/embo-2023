
#include "i2s.hh"
#include "audio_i2s.pio.h"
#include "hardware/pio.h"
#include "hardware/gpio.h"
#include "hardware/dma.h"
#include "hardware/irq.h"
#include "hardware/clocks.h"


bool pullPcmFrame(PcmFrame &);

extern "C"
{
#if BUILD_WITH_SDK
static void __isr __time_critical_func(audio_i2s_dma_irq_handler)();
#endif
} // extern "C"


namespace
{
constexpr uint32_t silenceFillLength = 128;

constexpr uint32_t i2sDmaIrq = 0;
// currently we only support stereo in, stereo out, 16 bit
constexpr auto i2sDmaWidth = DMA_SIZE_32;

PIO i2sPio = pio0;
constexpr uint32_t pioSm = 0;
constexpr uint32_t i2sDmaChannel = 0;

constexpr uint32_t silence = 0;
} // unnamed namespace


void i2sSetup(AudioI2sConfig const *cfg, AudioConfig const *fmt)
{
    auto func = GPIO_FUNC_PIO0;
    gpio_set_function(cfg->dataOutPin, func);
    gpio_set_function(cfg->bClkPin, func);
    gpio_set_function(cfg->bClkPin + 1, func);

    uint offset = pio_add_program(i2sPio, &audio_i2s_program);

    audio_i2s_program_init(i2sPio, pioSm, offset,
                           cfg->dataOutPin, cfg->bClkPin);

    uint32_t div = clock_get_hz(clk_sys) * 4 / fmt->sampleFreqHz;
    pio_sm_set_clkdiv_int_frac(i2sPio, pioSm, (div >> 8), (div & 0xff));

    dma_channel_config dmaCfg = dma_channel_get_default_config(i2sDmaChannel);

    channel_config_set_dreq(&dmaCfg, DREQ_PIO0_TX0 + pioSm);
    channel_config_set_transfer_data_size(&dmaCfg, i2sDmaWidth);
    dma_channel_configure(i2sDmaChannel,
                          &dmaCfg,
                          &i2sPio->txf[pioSm],  // dest
                          NULL, // src
                          0, // count
                          false // trigger
    );
#if BUILD_WITH_SDK
    irq_add_shared_handler(DMA_IRQ_0,
                           audio_i2s_dma_irq_handler,
                           PICO_SHARED_IRQ_HANDLER_DEFAULT_ORDER_PRIORITY);
#endif
    dma_irqn_set_channel_enabled(i2sDmaIrq, i2sDmaChannel, 1);
}

void i2sStart()
{
    irq_set_enabled(DMA_IRQ_0, true);

    // pre-fill DMA
    dma_channel_config c = dma_get_channel_config(i2sDmaChannel);
    channel_config_set_read_increment(&c, false);
    dma_channel_set_config(i2sDmaChannel, &c, false);
    dma_channel_transfer_from_buffer_now(i2sDmaChannel, &silence, silenceFillLength);

    pio_sm_set_enabled(i2sPio, pioSm, true);
}

PcmFrame irqFrame;

extern "C"
{
#if BUILD_WITH_SDK
void __isr __time_critical_func(audio_i2s_dma_irq_handler)()
#else
//[[gnu::section(".time_critical")]] void DMA0_IRQHandler()
void DMA0_IRQHandler()
#endif
{
    if (!dma_irqn_get_channel_status(i2sDmaIrq, i2sDmaChannel))
    {
        return;
    }
    dma_irqn_acknowledge_channel(i2sDmaIrq, i2sDmaChannel);

    if (!pullPcmFrame(irqFrame))
    {
        // fill with some silence
        dma_channel_config c = dma_get_channel_config(i2sDmaChannel);
        channel_config_set_read_increment(&c, false);
        dma_channel_set_config(i2sDmaChannel, &c, false);
        dma_channel_transfer_from_buffer_now(i2sDmaChannel, &silence, silenceFillLength);
        return;
    }

    dma_channel_config c = dma_get_channel_config(i2sDmaChannel);
    channel_config_set_read_increment(&c, true);
    dma_channel_set_config(i2sDmaChannel, &c, false);
    dma_channel_transfer_from_buffer_now(i2sDmaChannel,
                                         irqFrame.samples, irqFrame.sampleCnt);
}
} // extern "C"
