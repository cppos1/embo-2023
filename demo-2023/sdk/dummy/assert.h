#ifndef DUMMY_ASSERT_H_INCLUDES
#define DUMMY_ASSERT_H_INCLUDES

#ifdef __cplusplus
extern "C" {
#endif

static inline void assert(int cond)
{
    while (!cond)
        ;
}

#ifdef __cplusplus
} // extern "C"
#else
#define static_assert _Static_assert
#endif

#endif /* DUMMY_ASSERT_H_INCLUDES */
