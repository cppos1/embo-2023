#ifndef DUMMY_HARDWARE_CLOCKS_INCLUDED
#define DUMMY_HARDWARE_CLOCKS_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum clock_index {
    clk_gpout0 = 0,     ///< GPIO Muxing 0
    clk_gpout1,         ///< GPIO Muxing 1
    clk_gpout2,         ///< GPIO Muxing 2
    clk_gpout3,         ///< GPIO Muxing 3
    clk_ref,            ///< Watchdog and timers reference clock
    clk_sys,            ///< Processors, bus fabric, memory, memory mapped registers
    clk_peri,           ///< Peripheral clock for UART and SPI
    clk_usb,            ///< USB clock
    clk_adc,            ///< ADC clock
    clk_rtc,            ///< Real time clock
    CLK_COUNT
};

uint32_t clock_get_hz(enum clock_index clk_index);

#ifdef __cplusplus
}
#endif
#endif /* DUMMY_HARDWARE_CLOCKS_INCLUDED */
