#include <cppos/schedhelper.hh>

#include <cppos/port.hh>
#include <port-header.hh>

#include <cppos/scheduler.tt>


namespace cppos::impl
{
class sleeper : public event
{
public:
#if 0
    bool check_wakeup(task_block *) override
    {
        // I'm not sure we need this
        return true;
    }
#endif

    void check_for_wakeup()
    {
        for_all_blocked_tasks_check_unblock(
            [this] (task_block &t)
            {
                if (t.blocked_on == this && t.timeout < timer_ticks)
                {
                    t.state = task_state::ready;
                    t.blocked_on = nullptr;

                    return true;
                }

                return false;
            });
    }
};

sleeper sleep_obj;

void sched_helper::yield_for_sleep(uint32_t end)
{
    port::disable_interrupts();
    exec_context::sys_sched->current_task->timeout = end;
    exec_context::sys_sched->current_task->blocked_on = &sleep_obj;
    exec_context::sys_sched->current_task->state = task_state::blocked;
    sleep_obj.block(exec_context::sys_sched->current_task->idx);

    port::yield();
}
} // namespace cppos::impl

// for calls from assembler
extern "C"
{
using namespace cppos::impl;

void schedule_from_tick()
{
    ++timer_ticks;
    sleep_obj.check_for_wakeup();
    cppos::exec_context::sys_sched->schedule();
}

[[gnu::weak]] void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"
