
#include "port-hardware.hh"

#include "systraits.hh"
#include "cppos/irqContext.hh"

#include <array>

namespace cppos::impl
{
std::array<OpBase *, port_traits::irqCnt> isrTable;
} // namespace cppos::impl


extern "C"
{
[[gnu::signal,gnu::used,gnu::externally_visible]]
void TIMER3_COMPA_vect()
{
    cppos::impl::OpBase *isr = cppos::impl::isrTable[TIMER3_COMPA_vect_num];
    isr->f(isr);
}
} // extern "C"
