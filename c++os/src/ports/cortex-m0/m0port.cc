
#include <cppos/port.hh>
#include <systraits.hh>
#include <port-header.hh>

#include <cppos/scheduler.hh>

#include <bit>
#include <cstdint>
#include <chrono>

// here we need core_cm0(plus).h
// but this can't be included directly
// so we include here a header that must be provided for the specific MCU
// and must include that header for us
#include <mcu_core_cm.h>


extern "C"
{
extern void volatile * volatile * volatile previous_task_sp;
extern void volatile * volatile * volatile current_task_sp;

constexpr uint32_t exc_return_thread_psp = 0xffff'fffd;
constexpr uint32_t xpsr_thumb = 0x0100'0000;
constexpr uint32_t control_process_stack = 2;

// everything exported by the linker is an address to an object for the compiler
// to make it a pointer, declare (any) object and take the address of it
// this is RP2040 specific!
// we use __StackTop for ISRs and __StackLimit for program tasks
//extern std::byte __StackTop;
//std::byte *estack = &__StackTop;
extern std::byte __StackLimit;
std::byte *estack = &__StackLimit;
}

namespace
{
void setup_system_timer()
{
    if constexpr (cppos::port_traits::sys_timer_value != 0)
    {
        SysTick->LOAD = cppos::port_traits::sys_timer_value;
        NVIC_SetPriority(SysTick_IRQn, 0);
        SysTick->VAL = 0;
        SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk
            | SysTick_CTRL_TICKINT_Msk
            | SysTick_CTRL_ENABLE_Msk;
    }
    else
    {
        cppos::port_traits::sys_timer_setup();
    }
}
} // unnamed namespace

namespace cppos::impl
{
extern uint32_t volatile timer_ticks;
} // namespace cppos::impl

namespace cppos::port
{
using std::bit_cast;

void volatile *make_stack(void volatile *stack_top,
                          void *return_addr,
                          void *function_addr,
                          void *parameter)
{
    void **sp = (void **)(stack_top);

    // push initial status register
    *(--sp) = bit_cast<void *>(xpsr_thumb);

    // push (aligned) function address (popped to PC)
    *(--sp) = bit_cast<void *>(bit_cast<uint32_t>(function_addr) & 0xfffffffe);

    // address to return to if the function exits (popped to LR)
    *(--sp) = bit_cast<void *>(bit_cast<uint32_t>(return_addr) & 0xfffffffe);

    // space for R1, R2, R3, R12
    sp -= 4;

    // parameter
    *(--sp) = parameter;

    // space for R4 - R11
    sp -= 8;

    return sp;
}

void yield()
{
    exec_context::sys_sched->schedule();
    if (previous_task_sp != current_task_sp)
    {
        SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
    }

    // we need to enable interrupts always
    // this should be ok even if another interrupt is pending
    // that may cause scheduling
    // but a better option might be to call schedule()
    // from inside the PendSV ISR...
    enable_interrupts();
}

[[noreturn]] void initial_switch()
{
    setup_system_timer();
    __set_PSP(*(std::bit_cast<uint32_t *>(previous_task_sp)));
    __set_CONTROL(control_process_stack);

    SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
    enable_interrupts();

    while (true)
    {
        if (not interrupts_enabled())
        {
            error_handler();
        }
    }
}
} // namespace cppos::port

std::chrono::steady_clock::time_point
std::chrono::steady_clock::now() noexcept
{
    return time_point{chrono::milliseconds(cppos::impl::timer_ticks)};
}
