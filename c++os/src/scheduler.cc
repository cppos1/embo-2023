#include <cppos/scheduler.hh>

#include <systraits.hh>
#include <cppos/helpers.hh>
#include <cppos/port.hh>
#include <port-header.hh>

#include <cstdint>
#include <array>
#include <cstring>
#include <bit>

#include <cppos/scheduler.tt>

#define DBG_UART0 0
#define DBG_UART1 0
#if DBG_UART0
void dbgChar(char val);
void dbgInt(int line, unsigned val);
void dbgIntSync(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
#define LINTS(x) dbgIntSync(__LINE__, x)
#define LCHAR(c) (dbgChar(c))
#elif DBG_UART1
void uart1Int(int line, unsigned val);
void uart1IntSync(int line, unsigned val);
void uart1Char(char val);
#define LINT(x) uart1Int(__LINE__, x)
#define LINTS(x) uart1IntSync(__LINE__, x)
#define LCHAR(c) (uart1Char(c))
#else
#define LCHAR(c) (void)0
#define LINT(x) (void)0
#define LINTS(x) (void)0
#endif
namespace
{
using namespace cppos;

void idle()
{
    //LCHAR('\n');
    //LCHAR('I');
    uint32_t cnt = 0;

    while (1)
    {
        //uart1Char('i');
        // probably WFI
        //__asm volatile ("wfi");

        ++cnt;
        if ((cnt & 0x3ffff) == 1)
        {
            //LCHAR('i');
        }
    }
}

// I don't think this must be extern "C"
// as we don't care about the mangled name
void f_call_wrapper(nullary_function *f)
{
    (*f)();
}

//event selectEv;
} // unnamed namespace

namespace cppos
{
} // namespace cppos

// for calls from assembler
extern "C"
{
extern std::byte *estack;  // should be provided by linker script

void volatile * volatile * volatile previous_task_sp = nullptr;
void volatile * volatile * volatile current_task_sp = nullptr;
} // extern "C"

namespace cppos
{
namespace impl
{
uint32_t volatile timer_ticks = 0;

// by default all are on core0
std::array<uint8_t, task_count> taskCores;

nullary_function coreBaseFunction[port_traits::used_cores];
} // namespace impl

std::bitset<sys_traits::max_event_count> event::unblocked;
uint8_t event::curEventCnt = 0;
//std::array<task_block *, impl::task_count> event::allTasks;

exec_context *exec_context::sys_sched = nullptr;


event::event()
  : id_(curEventCnt++)
{
    allEvents[id_] = this;
}

bool event::unblock_all()
{
    unblocked[id_] = 1;
    uint8_t own_prio = (exec_context::sys_sched) ?
        exec_context::sys_sched->current_task->priority : 0;
    bool result = false;
    for_all_blocked_tasks_check_unblock(
        [own_prio, &result] (task_block &t)
        {
            t.state = task_state::ready;
            t.blocked_on = nullptr;

            if (t.priority > own_prio)
            {
                result = true; // yes, unblock
            }
            return true;
        });

    return result;
}

bool event::unblock_one()
{
    task_block *toUnblock = findUnblock();
    if (toUnblock)
    {
        return doUnblock(toUnblock);
    }

    return false;
}

task_block *event::findUnblock()
{
    uint8_t max_prio;
    task_block *to_unblock = nullptr;
    uint8_t blockedCnt = 0;

    for_all_blocked_tasks_check_unblock(
        [&max_prio, &to_unblock, &blockedCnt] (task_block &t)
        {
            ++blockedCnt;
            if (to_unblock == nullptr || t.priority > max_prio)
            {
                to_unblock = &t;
                max_prio = t.priority;
            }

            return false; // don't unblock yet
        });

    if (blockedCnt < 2)
    {
        unblocked[id_] = 1;
    }
    else
    {
        LINTS(id_);
    }

    return to_unblock;
}

bool event::doUnblock(task_block *t)
{
    bool result = false;
    if (exec_context::sys_sched)
    {
        if (t->priority > exec_context::sys_sched->current_task->priority)
        {
            result = true;
        }
    }

    blocked[t->idx] = false;
    t->state = task_state::ready;
    t->blocked_on = nullptr;

    return result;
}

#if 0
void event::addTask(task_block *t)
{
    allTasks[t->idx] = t;
}
#endif

void event::blockAll(task_block *tsk,
                    std::bitset<sys_traits::max_event_count> evs)
{
    for (size_t i = 0; i != evs.size(); ++i)
    {
        if (evs[i])
        {
            allEvents[i]->block(tsk->idx);
        }
    }
}


std::array<event *, sys_traits::max_event_count> event::allEvents;


exec_context::exec_context()
  : assigned_stack_top(estack - sys_traits::main_stack_size)
{
    // idle task should go to end of array, later...
    add(nullary_function(idle), 255, sys_traits::min_stack_size, 0);

    // we only support a single scheduler (at least for now)
    if (sys_sched)
    {
        error_handler();
    }
    sys_sched = this;

    // our start function
    impl::coreBaseFunction[0] = nullary_function{[this] { start(); }};
}

void exec_context::preBlock(event &ev)
{
    task_block &tsk = *(sys_sched->current_task);
    ev.block(tsk.idx);
    tsk.state = task_state::blocked;
}

void exec_context::finalBlock(event &)
{
    // if interrupts are already disabled this doesn't hurt
    // but generally an ISR should not run inside exec_context
    // and should never block anyways
    port::disable_interrupts();
    port::yield();
}

void exec_context::blockEvent(event &ev)
{
    task_block &tsk = *(sys_sched->current_task);
    ev.block(tsk.idx);
}

void exec_context::unblock(event &ev, task_block &unblockTask, bool doYield)
{
    if constexpr (port_traits::used_cores > 1)
    {
        port::sendEvent(ev, impl::taskCores[unblockTask.idx]);
    }

    if (doYield)
    {
        port::yield();
    }
}

void exec_context::yield()
{
    cppos::port::disable_interrupts();
    cppos::port::yield();
}

#if 1
void dbgChar(size_t tsk, char val)
{
    if (tsk != 0)
    {
        LCHAR(val);
    }
}
#endif

std::bitset<sys_traits::max_event_count>
exec_context::select(std::bitset<sys_traits::max_event_count> evs)
{
    //dbgChar(1, 'S');
    cppos::port::disable_interrupts();
    cppos::task_block *me = cppos::exec_context::sys_sched->current_task;
    //LINTS(me->idx);
    //event::blockAll(me, evs);

    me->selectMask = evs;
    if (evs.any())
    {
        me->state = cppos::task_state::blocked;
    }
    cppos::port::yield();
    //LINTS(me->idx);
    //dbgChar(me->idx, 's');

    return evs & event::getUnblocked();
}

[[noreturn]] void exec_context::start()
{
    current_task = &(impl::allTasks[0]);
    current_run = next_idx;
    schedule();
    // we need to keep the main stack for interrupt handlers (MSP)
    port::initial_switch();
}

void exec_context::schedule()
{
#if 0
    //if (&(tasks[0]) != current_task) LINT((uint32_t)current_task);
    if (current_task != &(tasks[0])
        && current_task != &(tasks[1])
        && current_task != &(tasks[2])
        && current_task != &(tasks[3])
       )
    {
        LINTS((uint32_t)current_task);
        while (1) ;
    }
#endif
    previous_task_sp = &(current_task->stack);
    size_t cur_idx = current_task->idx;
    //dbgChar(cur_idx, 's');
    //if (cur_idx != 0) LINT1(cur_idx);
    size_t next = cur_idx;
    unsigned prio = current_task->priority;
    uint8_t sub_prio = current_task->last_run;
    if (current_task->state == task_state::blocked)
    {
        next = 0; // idle task
        prio = 0;
        sub_prio = 0;
    }

    for (size_t i = cur_idx + 1; ; ++i)
    {
        if (i == next_idx)
        {
            i = 0;
        }

        if (i == cur_idx)
        {
            break;
        }

        task_block &tsk = impl::allTasks[i];

        if ((tsk.state == task_state::blocked) && tsk.selectMask.any())
        {
            //dbgChar(cur_idx, 'u');
            auto unblocked = event::getUnblocked();
            unblocked &= tsk.selectMask;
            if (unblocked.any())
            {
                //dbgChar(cur_idx, 'U');
                tsk.state = task_state::ready;
                tsk.selectMask.reset();
            }
        }

        if (tsk.state != task_state::ready)
        {
            dbgChar(cur_idx, 'b');
            continue;
        }

        // this is round robin backwards
        // if prios are the same, the last task is chosen
        if (tsk.priority < prio)
        {
            dbgChar(cur_idx, 'p');
            continue;
        }

        if (tsk.priority == prio)
        {
            uint8_t diff = impl::allTasks[i].last_run - sub_prio;
            if (diff < 0x7f)
            {
                dbgChar(cur_idx, 'd');
                continue;
            }
        }

        next = i;
        prio = tsk.priority;
        sub_prio = tsk.last_run;
    }

    if (cur_idx != next)
    {
        LCHAR('n');
        if (current_task->state == task_state::running)
        {
            current_task->state = task_state::ready;
        }
        current_task = &(impl::allTasks[next]);
        current_task->state = task_state::running;

#if 1

    LINT(next);
    LINT((uint32_t)current_task);
    LINT(current_task->id);
    LCHAR('x');
    LCHAR('\r');
    LCHAR('\n');
#endif
    }
    current_task->last_run = current_run++;

    auto stBot = static_cast<uint32_t volatile *>(current_task->stack_bottom);
    if (*stBot != sys_traits::default_stack_guard_value)
    {
        LINTS((uint32_t)stBot);
        LINTS(*stBot);
        while (1) ;
    }

    current_task_sp = &(current_task->stack);
}

void exec_context::add(nullary_function &&f,
                       uint8_t id,
                       int stack_size,
                       uint8_t prio)
{
    // @todo: we need to align the stack address to 32bit
    memset(assigned_stack_top - stack_size, 0xf0, stack_size);

    task_block &new_task = impl::allTasks[next_idx];
    LINTS(id);
    LINTS((uint32_t)&new_task);
    new_task.idx = next_idx;
    //event::addTask(&impl::allTasks[next_idx]);
    ++next_idx;

    new_task.init_foo = std::move(f);

    new_task.stack_end = assigned_stack_top;

    assigned_stack_top -= stack_size;
    new_task.stack_bottom = assigned_stack_top;
    LINTS((uint32_t)assigned_stack_top);

    memset(assigned_stack_top, 0xf0, stack_size);
    memcpy(assigned_stack_top,
           &sys_traits::default_stack_guard_value,
           sys_traits::default_stack_guard_size);

    new_task.stack = port::make_stack(new_task.stack_end,
                                      std::bit_cast<void *>(&error_handler),
                                      std::bit_cast<void *>(&f_call_wrapper),
                                      &new_task.init_foo);
    [[maybe_unused]] auto stBot = static_cast<uint32_t volatile *>(new_task.stack_bottom);
    LINTS((uint32_t)stBot);
    LINTS(*stBot);

    new_task.priority = prio;
    new_task.id = id;
    // we need distinct values for last_run, the actual value doesn't matter
    new_task.last_run = next_idx;
}

void startContexts()
{
    cppos::port::enable_interrupts();

    for (size_t i = 0; i != port_traits::used_cores; ++i)
    {
        impl::coreBaseFunction[i]();
    }
}

std::array<task_block, impl::task_count> impl::allTasks;
} // namespace cppos
