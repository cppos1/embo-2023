#ifndef _COMMON_ATOMICS_INCLUDED
#define _COMMON_ATOMICS_INCLUDED
#pragma GCC system_header

#include <port-header.hh>

#include <functional> // for plus/minus

// we try to get away without all the sized overloads
// we generally ignore the memory order, we're always seq_cst

#define __atomic_load_n(__ptr, __mem_order) \
    cppos::port::atomic_impl::own_atomic_load_n(__ptr)
#define __atomic_load(__ptr, __ret, __mem_order)      \
    cppos::port::atomic_impl::own_atomic_load(__ptr, __ret)

#define __atomic_store_n(__ptr, __val, __mem_order)   \
    cppos::port::atomic_impl::own_atomic_store_n(__ptr, __val)
#define __atomic_store(__ptr, __val, __mem_order)      \
    cppos::port::atomic_impl::own_atomic_store(__ptr, __val)

#define __atomic_fetch_add(__ptr, __val, __mem_order)   \
    cppos::port::atomic_impl::own_atomic_fetch_add(__ptr, __val)
#define __atomic_add_fetch(__ptr, __val, __mem_order) \
    cppos::port::atomic_impl::own_atomic_add_fetch(__ptr, __val)

#define __atomic_fetch_sub(__ptr, __val, __mem_order)   \
    cppos::port::atomic_impl::own_atomic_fetch_sub(__ptr, __val)
#define __atomic_sub_fetch(__ptr, __val, __mem_order) \
    cppos::port::atomic_impl::own_atomic_sub_fetch(__ptr, __val)

#define __atomic_exchange_n(__ptr, __des, __mem_order) \
    cppos::port::atomic_impl::own_atomic_exch(__ptr, __des)

#define __atomic_compare_exchange_n(__ptr, __exp, __des, __weak, __mosucc, __mofail) \
    cppos::port::atomic_impl::own_atomic_cmp_exch(__ptr, *__exp, __des)

namespace cppos::port::atomic_impl
{
// helper concepts for small types
template <typename T>
concept HasAtomicLoadStore = (sizeof(T) <= nativeAtomicSize);
template <typename T>
concept HasNoAtomicLoadStore = (sizeof(T) > nativeAtomicSize);

template <typename T>
struct LoadStoreLock;

template <HasAtomicLoadStore T>
struct LoadStoreLock<T>
{
};

#ifndef HAS_ATOMIC_EXTRA_LOCK
#define HAS_ATOMIC_EXTRA_LOCK interrupt_lock
#endif
typedef HAS_ATOMIC_EXTRA_LOCK LockHelper;

template <HasNoAtomicLoadStore T>
struct LoadStoreLock<T>
{
    [[gnu::always_inline]] LoadStoreLock() = default;
    [[gnu::always_inline]] ~LoadStoreLock() = default;

    [[no_unique_address]] LockHelper lock;
};


template <typename T, typename Ot, class Op>
struct DelayedOp
{
    [[gnu::always_inline]] DelayedOp(T & obj, Ot val)
      : object(obj),
        operand(val)
    {
    }

    [[gnu::always_inline]] ~DelayedOp()
    {
        object = (Op())(object, operand);
    }

    T &object;
    [[no_unique_address]] Ot operand;
};

template <typename T>
[[gnu::always_inline]] T own_atomic_load_n(T const volatile *ptr)
{
    [[no_unique_address]] LoadStoreLock<T> lh;
    return *ptr;
}

template <typename T>
[[gnu::always_inline]]
void own_atomic_load(T const volatile *ptr, T *ret)
{
    [[no_unique_address]] LoadStoreLock<T> lh;
    *ret = *ptr;
}

template <typename T>
[[gnu::always_inline]]
void own_atomic_store_n(T volatile *ptr, T val)
{
    [[no_unique_address]] LoadStoreLock<T> lh;
    *ptr = val;
}

template <typename T>
[[gnu::always_inline]]
void own_atomic_store(T volatile *ptr, T const *val)
{
    [[no_unique_address]] LoadStoreLock<T> lh;
    *ptr = *val;
}

// atomic_base uses unqualified '1' for ++ and -- (which is probably correct)
template <typename T, typename OT>
[[gnu::always_inline]] T own_atomic_add_fetch(T volatile *ptr, OT val)
{
    [[no_unique_address]] LockHelper lh;
    return *ptr += val;
}

template <typename T, typename OT>
[[gnu::always_inline]] T own_atomic_fetch_add(T volatile *ptr, OT val)
{
    [[no_unique_address]] LockHelper lh;
    [[no_unique_address]] DelayedOp<T, OT, std::plus<T>> dop(*ptr, val);
    return *ptr;
}

template <typename T, typename OT>
[[gnu::always_inline]] T own_atomic_sub_fetch(T volatile *ptr, OT val)
{
    [[no_unique_address]] LockHelper lh;
    return *ptr -= val;
}

template <typename T, typename OT>
[[gnu::always_inline]] T own_atomic_fetch_sub(T volatile *ptr, OT val)
{
    [[no_unique_address]] LockHelper lh;
    [[no_unique_address]] DelayedOp<T, OT, std::minus<T>> dop(*ptr, val);
    return *ptr;
}

template <typename T, typename OT>
[[gnu::always_inline]] T own_atomic_exch(T volatile *ptr,
                                            OT desired)
{
    [[no_unique_address]] LockHelper irh;
    T result = *ptr;
    *ptr = desired;
    return result;
}

template <typename T, typename OT>
[[gnu::always_inline]] bool own_atomic_cmp_exch(T volatile *ptr,
                                                T &expected,
                                                OT desired)
{
    [[no_unique_address]] LockHelper irh;
    if (*ptr == expected)
    {
        *ptr = desired;
        return true;
    }
    else
    {
        expected = *ptr;
        return false;
    }
}
} // namespace atomic_impl
#endif /* _COMMON_ATOMICS_INCLUDED */
