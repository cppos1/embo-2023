#ifndef CPPOS_ERRNO_H_INCLUDED
#define CPPOS_ERRNO_H_INCLUDED
#ifdef __AVR
#include <ports/avr/errno.h>
#else
#include_next <errno.h>
#endif
#endif /* CPPOS_ERRNO_H_INCLUDED */
