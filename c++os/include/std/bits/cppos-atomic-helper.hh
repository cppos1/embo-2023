#ifndef CPPOS_ATOMIC_HELPER_INCLUDED
#define CPPOS_ATOMIC_HELPER_INCLUDED

#if __has_include(<port-defs.hh>)
#include <port-defs.hh>
#endif

#ifdef __AVR
// avr-gcc doesn't have the atomic compiler intrinsics
#include <ports/avr/atomics.hh>
#endif

#ifdef __RP2040_PORT
// multi-core Cortex-M0 needs MCU specific software support
#include <ports/rp2040/atomics.hh>
#endif

#endif /* CPPOS_ATOMIC_HELPER_INCLUDED */
