#ifndef _SCHEDULER_HH_INCLUDED
#define _SCHEDULER_HH_INCLUDED
#include "systraits.hh"
#include "helpers.hh"

#include <cstdint>
#include <array>
#include <bitset>

// for calls from assembler
extern "C"
{
void schedule_from_tick();
[[noreturn]] void error_handler();

extern void volatile * volatile * volatile previous_task_sp;
extern void volatile * volatile * volatile current_task_sp;
} // extern "C"

namespace std
{
class mutex;
class condition_variable;
} // namespace std

namespace cppos
{
namespace impl
{
constexpr int task_count =
    sys_traits::static_task_count + sys_traits::dynamic_task_count + 1;

extern std::array<uint8_t, task_count> taskCores;

class sched_helper;

extern nullary_function coreBaseFunction[port_traits::used_cores];
} // namespace impl

namespace port
{
void yield();
} // namespace port

enum class task_state : uint8_t
{
    unused,
    blocked,
    ready,
    running,
};

void startContexts();

struct task_block;

class event
{
public:
    event();

    //virtual bool check_wakeup(task_block *) = 0;

    template <std::invocable<task_block &> _F>
    void for_all_blocked_tasks_check_unblock(_F foo);

    void block(size_t idx)
    {
        blocked[idx] = true;
        unblocked[id_] = false;
    }

#if 0
    void block();
#endif

    bool any() const // is any task blocked on this?
    {
        return blocked.any();
    }

    // return true if switch is required
    bool unblock_all();
    bool unblock_one();

    template <typename Exec> void notify_one()
    {
        task_block *toUnblock = findUnblock();
        if (toUnblock)
        {
            bool otherHasHigherPrio = doUnblock(toUnblock);
            Exec::unblock(*this, *toUnblock, otherHasHigherPrio);
        }
    }
    //template <typename Exec> void notify_all();

    uint8_t id()
    {
        return id_;
    }

    bool isBlocked(uint8_t taskIdx)
    {
        return blocked[taskIdx];
    }


#if 0
    static void setUnblocked(uint8_t evId)
    {
        doUnblock(allTasks[taskIdx]);
    }
#endif

    static const auto getUnblocked()
    {
        return unblocked;
    }

    //static void addTask(task_block *);

    static void blockAll(task_block *, std::bitset<sys_traits::max_event_count>);

protected:
    // ToDo: several members here probably need to be atomic
    // we need our own bitset to make the underlying bytes atomic
    // bitset can't be volatile either
    std::bitset<impl::task_count> blocked;
    uint8_t id_;

    static std::bitset<sys_traits::max_event_count> unblocked;
    static uint8_t curEventCnt;

    // ToDo: we misuse dynamic_task_count for non-exec_context tasks
    // we need a joined one for event handling
    // another option is to have a mechanism to register
    // local task lists somewhere
    //static std::array<task_block *, impl::task_count> allTasks;

private:
    task_block *findUnblock();
    bool doUnblock(task_block *);

    static std::array<event *, sys_traits::max_event_count> allEvents;
};

struct task_block
{
    void volatile *stack;
    void volatile *stack_end;
    void volatile *stack_bottom;
    // we probably don't need this, but for now we keep it
    event volatile *blocked_on = nullptr;
    std::bitset<sys_traits::max_event_count> selectMask;
    uint32_t volatile timeout = 0;

    /**
     * we never use init_foo directly, but we need a place to store it
     * it may be a lambda with capture
     */
    nullary_function init_foo;

    //size_t max_stack_size = 0;

    task_state volatile state = task_state::ready;
    uint8_t priority = sys_traits::default_priority;
    uint8_t id; // can be used by user for identification
    uint8_t idx = 255; // index in array
    uint8_t last_run = 0; // sub prio 
};

#if 0
std::bitset<sys_traits::max_event_count>
select(std::bitset<sys_traits::max_event_count>);
#endif

class exec_context
{
public:
    exec_context();
    exec_context(exec_context &&) = delete;

    template <std::invocable _F>
    void add_static(_F &&foo,
                    uint8_t id,
                    int stack_size = sys_traits::default_stack_size,
                    uint8_t prio = sys_traits::default_priority);

    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    auto scheduler();

    [[deprecated]] static void block(event &ev)
    {
        preBlock(ev);
        finalBlock(ev);
    }
    static void preBlock(event &ev);
    static void finalBlock(event &);
    static void blockEvent(event &ev);

    static void unblock(event &ev, task_block &unblockTask, bool);

    static void yield();

    static std::bitset<sys_traits::max_event_count>
    select(std::bitset<sys_traits::max_event_count>);

    [[noreturn]] void start();

private:
    friend class event;
    friend class impl::sched_helper;
    friend class std::mutex;
    friend class std::condition_variable;
    friend void ::schedule_from_tick();
    friend void cppos::port::yield();
    friend void startContexts();
    friend void volatile *debug_handle();

    void schedule();
    void add(nullary_function &&f, uint8_t id, int stack_size, uint8_t prio);

    task_block *current_task = nullptr;
    std::byte *assigned_stack_top;

    uint8_t next_idx = 0;
    uint8_t current_run = 0;

    static exec_context *sys_sched;
};

template <uint8_t TaskId,
          unsigned StackSize = sys_traits::default_stack_size,
          uint8_t Priority = sys_traits::default_priority>
class scheduler
{
public:
    scheduler(exec_context *ctx)
      : context(ctx)
    {}

    template <std::invocable Foo>
    void execute(Foo &&f)
    {
        context->add_static(std::forward<Foo>(f),
                            TaskId, StackSize, Priority);
    }

private:
    exec_context *context;
};

template <uint8_t TaskId,
          size_t StackSize,
          uint8_t Priority>
auto exec_context::scheduler()
{
    return cppos::scheduler<TaskId, StackSize, Priority>(this);
}

namespace impl
{
extern std::array<task_block, task_count> allTasks;
} // namespace impl
} // namespace cppos
#endif /* _SCHEDULER_HH_INCLUDED */
