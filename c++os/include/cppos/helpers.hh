#ifndef _HELPERS_HH_INCLUDED
#define _HELPERS_HH_INCLUDED

#include <cstdint>
#include <array>
#include <bit>
#include <concepts>

namespace cppos
{
class nullary_function_base
{
public:
    virtual void operator()() = 0;
    // no virtual destructor??

private:
};

class nullary_function
{
public:
    // it's UB to call a unary function without having it filled
    nullary_function() = default;
    // just copying/moving the array should be good enough
    //nullary_function(nullary_function &&) = default;
    template <std::invocable _F> explicit nullary_function(_F &&foo);
    template <std::invocable _F> void operator=(_F &&foo);

    void operator()()
    {
        // bit_cast not in GCC 10 :-(
        std::bit_cast<nullary_function_base *>(foo_storage.data())->operator()();
        //reinterpret_cast<nullary_function_base *>(foo_storage.data())->operator()();
    }

private:
    std::array<std::byte, sys_traits::max_function_size> foo_storage;
};
} // namespace cppos
#endif /* _HELPERS_HH_INCLUDED */
