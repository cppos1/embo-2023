#ifndef _PORT_HH_INCLUDED
#define _PORT_HH_INCLUDED
namespace cppos::port
{
void volatile *make_stack(void volatile *stack_top,
                          void *return_addr,
                          void *function_addr,
                          void *parameter);

void yield();

// start the very first task (after reset and setup)
[[noreturn]] void initial_switch();

// setup system timer interrupt
// can and must be done in initial_switch()
//void setup_system_timer();
} // namespace cppos::port
#endif /* _PORT_HH_INCLUDED */
