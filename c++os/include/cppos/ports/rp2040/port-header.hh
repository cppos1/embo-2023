#ifndef RP2040_PORT_HEADER_HH_INCLUDED
#define RP2040_PORT_HEADER_HH_INCLUDED

#define HAS_ATOMIC_EXTRA_LOCK atomic_lock

#include <cppos/ports/cortex-m0/port-header.hh>
#include <rp_regs.h>
#include <rp2040-helpers.hh>

namespace cppos
{
class event;

namespace port
{
void sendEvent(event &ev, uint32_t coreId);

template <uint32_t SpinLockNo>
struct spin_lock
{
    constexpr static helpers::Reg slReg{SIO_SPINLOCK0 + (SpinLockNo * 4)};

    [[gnu::always_inline]] spin_lock()
    {
        while (*slReg.ptr() == 0)
            ;
    }

    [[gnu::always_inline]] ~spin_lock()
    {
        *slReg.ptr() = 1;
    }
};

struct sync_lock
{
    [[gnu::always_inline]] sync_lock()
    {
        __atomic_thread_fence(__ATOMIC_SEQ_CST);
    }

    [[gnu::always_inline]] ~sync_lock()
    {
        __atomic_thread_fence(__ATOMIC_SEQ_CST);
    }
};

namespace atomic_impl
{
constexpr unsigned nativeAtomicSize = 4;

struct atomic_lock
{
    [[gnu::always_inline]] atomic_lock() = default;
    [[gnu::always_inline]] ~atomic_lock() = default;

    [[no_unique_address]] sync_lock syl;
    [[no_unique_address]] interrupt_lock il;
    [[no_unique_address]] spin_lock<port_traits::atomic_spinlock_no> sl;
};
} // namespace atomic_impl
} // namespace port
} // namespace cppos
#endif /* RP2040_PORT_HEADER_HH_INCLUDED */
