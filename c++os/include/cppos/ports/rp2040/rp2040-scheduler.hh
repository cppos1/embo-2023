#ifndef RP2040_SCHEDULER_HH_INCLUDED
#define RP2040_SCHEDULER_HH_INCLUDED
#include "systraits.hh"

#include "cppos/scheduler.hh"

#include "port-header.hh"
#include "rp2040.h"

#include <cstdint>
#include <cstddef>
#include <array>


#include "../lib/gpio.hh"
namespace
{
GpioOut<0, 26> led4(true);
}

namespace rp2040
{
void start_core1(void (*f)());
} // namespace rp2040

namespace cppos
{

namespace port
{
namespace impl
{
extern nullary_function core1Fun;

void core1Task();
} // namespace impl
} // namespace port

#if 0 // we just use our exec_context on core 0
template <size_t CoreNo>
class preempt_context
{
public:
    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    struct Scheduler
    {
        Scheduler(preempt_context *ctx)
          : context{ctx}
        {}

        template <std::invocable Foo>
        void execute(Foo &&f)
        {
            context->add_static(std::forward<Foo>(f),
                                TaskId, StackSize, Priority);
        }

        preempt_context *context;
    };

    preempt_context();
    preempt_context(preempt_context &&) = delete;

    template <std::invocable Foo>
    void add_static(Foo &&f,
                    uint8_t id,
                    size_t stack_size = sys_traits::default_stack_size,
                    uint8_t prio = sys_traits::default_priority)
    {
        impl::addTask(nullary_function(f), id, stack_size, prio);
    }

    // this probably shouldn't be static, but it should be ok for now
    static void block(event &ev)
    {
        ev.block();
    }

    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    auto scheduler()
    {
        return Scheduler<TaskId, StackSize, Priority>(this);
    }

private:
    friend void startContexts();

    template <std::invocable Foo>
    void add_static(Foo &&f,
                    uint8_t id,
                    size_t stack_size = sys_traits::default_stack_size,
                    uint8_t prio = sys_traits::default_priority)
    {
        impl::task_block &t = tasks[curTaskCnt];
        t.foo = std::forward<Foo>(f);
        t.frHandle = nullptr;
        t.stackSize = stack_size;
        t.id = id;
        t.priority = prio;

        ++curTaskCnt;

        if (started)
        {
            startTask(t);
        }
    }

    void start();
    void startTask(impl::task_block &);

    int core;
    std::array<impl::task_block, sys_traits::static_task_count> tasks;
    size_t curTaskCnt;
    bool started = false;
};
#endif

template <size_t CoreNo>
class single_context
{
public:
    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    struct Scheduler
    {
        Scheduler(single_context *ctx)
          : context{ctx}
        {}

        template <std::invocable Foo>
        void execute(Foo &&f)
        {
            // we need to store the function and only start it on startContexts
            if constexpr (CoreNo == 0)
            {
                // for core0 we can just store the function
                impl::coreBaseFunction[CoreNo] = nullary_function(f);
            }
            else
            {
                // for core1 we need our start_core1
                port::impl::core1Fun = f;
                rp2040::start_core1(port::impl::core1Task);
            }
        }

        single_context *context;
    };

    single_context()
    {
        myTask.stack = 0;
        myTask.stack_end = 0;
        myTask.stack_bottom = 0;
        myTask.blocked_on = nullptr;
        myTask.timeout = 0;
        myTask.state = task_state::ready;
        myTask.id = 99;
        // this is a hack, we need a better way for our task idx
        myTask.idx = sys_traits::static_task_count + CoreNo;

        impl::taskCores[myTask.idx] = CoreNo;
    }

    single_context(single_context &&) = delete;


    template <uint8_t TaskId,
              size_t StackSize = sys_traits::default_stack_size,
              uint8_t Priority = sys_traits::default_priority>
    auto scheduler()
    {
        return Scheduler<TaskId, StackSize, Priority>(this);
    }

    // this probably shouldn't be static, but it should be ok for now
    static void preBlock(event &e)
    {
        e.block(myTask.idx);
        // this may not be used
        myTask.state = task_state::blocked;
    }

    static void finalBlock(event &e)
    {
        while (e.isBlocked(myTask.idx))
        {
#if 1
            port::interrupt_lock l;
            // we need to check the event again
            if (not e.isBlocked(myTask.idx))
            {
                return;
            }
            // check for FIFO IRQ (SIO_IRQ_PROC0_IRQn + CoreNo)
            if (SIO->FIFO_ST_b.VLD)
            {
                // we let the interrupt arrive and check again
                continue;
            }
            __asm volatile ("wfi");
#else
            __NOP();
#endif
        }
    }

    static void blockEvent(event &e)
    {
        e.block(myTask.idx);
    }

    static void unblock(event &e, task_block &unblockTask, bool)
    {
        if (CoreNo != impl::taskCores[unblockTask.idx])
        {
            // send event to other core
            SIO->FIFO_WR = reinterpret_cast<uint32_t>(&e);
        }
        else
        {
            // nothing to do
        }
    }

    static void yield()
    {
        ; // just return
    }

    std::bitset<sys_traits::max_event_count>
    static select(std::bitset<sys_traits::max_event_count> evs)
    {
        auto resultSet = evs & event::getUnblocked();
        if (resultSet.any())
        {
            return resultSet;
        }

        event::blockAll(&myTask, evs);
        //myTask->state = cppos::task_state::blocked;

        while ((evs & event::getUnblocked()).none())
        {
#if 0
            port::interrupt_lock l;
            // we need to check the event again
            resultSet = evs & event::getUnblocked();
            if (resultSet.any())
            {
                break;
            }
            // check for FIFO IRQ (SIO_IRQ_PROC0_IRQn + CoreNo)
            if (SIO->FIFO_ST_b.VLD)
            {
                // we let the interrupt arrive and check again
                continue;
            }
            __asm volatile ("wfi");
#else
            __NOP();
#endif
        }
        led4.toggle();

        return evs & event::getUnblocked();
    }

private:
    static task_block &myTask;
};

#if 0
template <size_t CoreNo>
task_block single_context<CoreNo>::myTask =
{
    .stack = 0,
    .stack_end = 0,
    .stack_bottom = 0,
    .blocked_on = nullptr,
    .timeout = 0,
    .state = task_state::ready,
    .id = 99,
    // this is a hack, we need a better way for our task idx
    .idx = sys_traits::static_task_count + CoreNo,
};
#else
template <size_t CoreNo>
task_block &single_context<CoreNo>::myTask =
    impl::allTasks[sys_traits::static_task_count + CoreNo];
#endif
} // namespace cppos
#endif /* RP2040_SCHEDULER_HH_INCLUDED */
