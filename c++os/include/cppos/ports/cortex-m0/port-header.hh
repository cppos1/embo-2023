#ifndef CORTEX_M0_PORT_HEADER_HH_INCLUDED
#define CORTEX_M0_PORT_HEADER_HH_INCLUDED

// we need the CMSIS functions like __enable_irq
// for CMSIS5, these are in cmsis_gcc.h
// but this is not always compatible with SDKs for chips that use older
// CMSIS versions
// so we include here a header that must be provided for the specific MCU
// and must provide definitions for these functions/macros
#include <mcu_cmsis_func.h>

#include <systraits.hh>

namespace cppos
{
namespace port
{
typedef unsigned native_ut;

struct interrupt_lock
{
    [[gnu::always_inline]] interrupt_lock()
      : primask(__get_PRIMASK())
    {
        __disable_irq();
    }
    [[gnu::always_inline]] ~interrupt_lock()
    {
        if (!unlocked && !primask)
        {
            __enable_irq();
        }
    }

    [[gnu::always_inline]] void unlock()
    {
        if (!primask)
        {
            __enable_irq();
        }
        unlocked = true;
    }

    uint32_t primask;
    bool unlocked = false;
};

[[gnu::always_inline]] inline void disable_interrupts()
{
    __disable_irq();
}

[[gnu::always_inline]] inline void enable_interrupts()
{
    __enable_irq();
}

[[gnu::always_inline]] inline bool interrupts_enabled()
{
    return not __get_PRIMASK();
}
} // namespace port
} // namespace cppos
#endif /* CORTEX_M0_PORT_HEADER_HH_INCLUDED */
