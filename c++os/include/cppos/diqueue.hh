#pragma once
#ifndef CPPOS_DIQUEUE_HH_INCLUDED
#define CPPOS_DIQUEUE_HH_INCLUDED

#include <port-header.hh>
#include "queue.hh"
#include "scheduler.hh"

#include <utility>
#include <atomic>

namespace cppos
{
namespace impl
{
template <typename T, size_t QSize> class InternalQ;
} // namespace impl

// trimmed down and modified from p0260r2 (2017-10)
// this version uses interrupt disabling for synchronization

template <typename Value, size_t Size>
class di_queue
{
    di_queue(const di_queue&) = delete;
    di_queue& operator =(const di_queue&) = delete;

public:
    typedef Value value_type;

    explicit di_queue()
      : cnt{0}
      , head{0}
      , tail{0}
      , closed{false}
    {}

    ~di_queue() noexcept = default;

    void close() noexcept
    {
        closed = true;

        notEmpty.unblock_all();
        notFull.unblock_all();
    }

    queue_op_status wait_pop(Value &v)
    {
        queue_op_status ret = queue_op_status::success;
        port::disable_interrupts();

        while (cnt == 0 && !closed)
        {
            notEmpty.block();
            port::yield();
            // after return from yield() interrupts are enabled
            port::disable_interrupts();
        }

        if (cnt != 0)
        {
            v = std::move(buf[head]);
            --cnt;
            head = (head+1) % Size;
        }
        else
        {
            ret = queue_op_status::closed;
        }

        port::enable_interrupts();
        notFull.unblock_one();

        return ret;
    }

    queue_op_status try_pop(Value &v)
    {
        port::interrupt_lock l;

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == 0)
        {
            return queue_op_status::empty;
        }

        v = std::move(buf[head]);
        --cnt;
        head = (head+1) % Size;

        notFull.unblock_one();

        return queue_op_status::success;
    }

    template <typename Vt>
    queue_op_status wait_push(Vt &&v)
    {
        queue_op_status ret = queue_op_status::success;
        port::disable_interrupts();

        while (cnt == Size && !closed)
        {
            notFull.block();
            port::yield();
            // after return from yield() interrupts are enabled
            port::disable_interrupts();
        }

        if (closed)
        {
            ret = queue_op_status::closed;
        }
        else
        {
            buf[tail] = std::forward<Vt>(v);
            ++cnt;
            tail = (tail+1) % Size;
        }

        port::enable_interrupts();
        notEmpty.unblock_one();

        return ret;
    }

    template <typename Vt>
    queue_op_status try_push(Vt &&v)
    {
        port::interrupt_lock l;

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == Size)
        {
            return queue_op_status::full;
        }

        buf[tail] = std::forward<Vt>(v);
        ++cnt;
        tail = (tail+1) % Size;

        notEmpty.unblock_one();

        return queue_op_status::success;
    }

private:
    friend class impl::InternalQ<Value, Size>;

    Value buf[Size];

    std::atomic<size_t> cnt;
    std::atomic<size_t> head;
    std::atomic<size_t> tail;
    std::atomic<bool> closed;

    event notEmpty;
    event notFull;
};
} // namespace cppos

#endif /* CPPOS_DIQUEUE_HH_INCLUDED */
