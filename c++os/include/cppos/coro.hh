#ifndef CPPOS_CORO_INCLUDED
#define CPPOS_CORO_INCLUDED

#include "systraits.hh"

#include <bitset>

#include <coroutine>


#include <uart.hh>
#include "rp2040.hh"
#include <charconv>
namespace
{
constexpr int printBuf2Size = 80;
extern char printBuf2[];

#if 0
void syncWrite(char const *str, size_t len)
{
    while (!uart1.done())
    {
        __NOP();
    }
    uart1.write(str, len);
}

void dbgInt1(int line, unsigned val)
{
    auto bufEnd = printBuf2 + printBuf2Size;
    char *end = printBuf2;

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';
    syncWrite(printBuf2, end - printBuf2);
}
#endif
} // unnamed namespace

#define DBG_UART0 0
#define DBG_UART1 0
#if DBG_UART0
void dbgChar(char val);
void dbgInt(int line, unsigned val);
void dbgIntSync(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
#define LINTS(x) dbgIntSync(__LINE__, x)
#define LCHAR(c) (dbgChar(c))
#elif DBG_UART1
void uart1Int(int line, unsigned val);
void uart1IntSync(int line, unsigned val);
void uart1Char(char val);
#define LINT(x) uart1Int(__LINE__, x)
#define LINTS(x) uart1IntSync(__LINE__, x)
#define LCHAR(c) (uart1Char(c))
#else
#define LCHAR(c) (void)0
#define LINT(x) (void)0
#define LINTS(x) (void)0
#endif

namespace cppos
{

// proof of concept only

#if 0
struct coroBlock
{
    std::bitset
};
#endif

class DummyPromise;
struct CoHandle
{
    typedef DummyPromise promise_type;
    typedef std::coroutine_handle<promise_type> CoHandleT;

    ~CoHandle()
    {
        handle.destroy();
    }

    CoHandleT handle;
};

class DummyPromise
{
    typedef std::suspend_always Suspend;
    typedef std::suspend_never NoSuspend;
public:
    NoSuspend initial_suspend()
    {
        return {};
    }
    NoSuspend final_suspend() noexcept
    {
        return {};
    }
    void return_void()
    {
    }
    CoHandle get_return_object()
    {
        return {CoHandle::CoHandleT::from_promise(*this)};
    }
    void unhandled_exception() noexcept
    {
    }

    // I don't think there's a way tomake this fail at link
    // time if not enough memory is available
    // even if size is available at link time
    // size is not constexpr
    // this function must be provided by the application
    void* operator new(size_t size) noexcept;
    void operator delete(void *) noexcept;

    static CoHandle get_return_object_on_allocation_failure()
    {
        return CoHandle{};
    }
};

template <class Exec>
class CoLoop
{
public:
    typedef Exec executor_type;

    void add(event &e, std::coroutine_handle<> coro)
    {
        //dbgInt1(__LINE__, (uintptr_t)(coro.address()));
        if (used.all())
        {
            //dbgInt1(__LINE__, 0);
            error_handler();
        }

        for (unsigned i = 0; i != tableSize; ++i)
        {
            if (!used[i])
            {
                LINT(reinterpret_cast<uintptr_t>(this));
                LINT(i);
                LINT(e.id());
                table[i] = std::pair(&e, coro);
                used[i] = true;
                Exec::blockEvent(e);
                break;
            }
        }
    }

    void remove(event &e, std::coroutine_handle<> coro)
    {
        std::pair p(&e, coro);
        for (unsigned i = 0; i != tableSize; ++i)
        {
            if (p == table[i])
            {
                LINT(i);
                used[i] = false;
            }
        }
    }

#if 0
    void dbgPrint(char c, std::bitset<sys_traits::max_event_count> evs)
    {
        auto bufEnd = printBuf2 + printBuf2Size;
        char *end = printBuf2;
        *(end++) = c;

        end = std::to_chars(end, bufEnd, tableSize).ptr;
        *(end++) = ' ';

        for (size_t i = 0; i != tableSize; ++i)
        {
            char c = '0';
            if (evs[i])
            {
                c = '1';
            }
            *(end++) = c;
            *(end++) = ' ';
        }

        *(end++) = '\r';
        *(end++) = '\n';
        uart1.write(printBuf2, end - printBuf2);
    }
#else
    void dbgPrint(char c, std::bitset<sys_traits::max_event_count> evs)
    {
    }
#endif

    void start()
    {
        LCHAR('L');
        int dbgCnt = 0;
        while (true)
        {
            //bool doDebug = (dbgCnt & 0x3) == 0;
            //bool doDebug = true;
            bool doDebug = false;
            ++dbgCnt;
            // setup bitset
            std::bitset<sys_traits::max_event_count> evs;
            for (size_t i = 0; i != tableSize; ++i)
            {
                if (used[i])
                {
                    LINT(reinterpret_cast<uintptr_t>(this));
                    LINT(i);
                    LINT(table[i].first->id());
                    evs[table[i].first->id()] = true;
                }
            }
            if (doDebug)
            {
                dbgPrint('P', evs);
            }

            auto unblockedEvs = Exec::select(evs);
            if (doDebug)
            {
                dbgPrint('A', unblockedEvs);
            }

            // handle all events
            // a resumed coroutine may block on the same event
            // so we first collect all coros to resume
            // and only after we actually resume them
            std::bitset<tableSize> corosToResume;

            for (size_t i = 0; i != sys_traits::max_event_count; ++i)
            {
                if (unblockedEvs[i])
                {
                    // ToDo: a back pointer is better than a nested loop
                    for (size_t j = 0; j != tableSize; ++j)
                    {
                        if (used[j] && table[j].first->id() == i)
                        {
                            corosToResume[j] = true;
                        }
                    }
                }
            }

            for (size_t i = 0; i != tableSize; ++i)
            {
                if (corosToResume[i])
                {
                    auto coro = table[i].second;
                    //dbgInt1(__LINE__, (uintptr_t)(coro.address()));
                    //dbgInt1(__LINE__, i);
                    //syncWrite("C", 1);
                    if (coro)
                    {
                        //syncWrite("c", 1);
                        // ToDo:
                        // this is too simple
                        // by the time the coroutine is resumed (now)
                        // somebody else might have come in
                        // so we probably need to store a reference to the
                        // awaitable and call something like
                        // possibly_resume() there that can check
                        // before the coroutine is really resumed
                        coro();
                    }
                }
            }
        }
    }

#if 0
    static void preBlock(event &ev)
    {
        if (not coros[ev.id])
        {
            Exec::preBlock(ev);
        }
    }

    static void finalBlock(event &ev)
    {
    }

    static void unblock(event &e, task_block &unblockTask, bool doYield)
    {
        auto c = coros[ev.id];
        if (c)
        {
            c();
        }
        else
        {
            Exec::unblock(ev, unblockTask, doYield);
        }
    }
#endif

private:
    // we can have multiple coroutines waiting for the same event!
    // (e.g. mutex)
    // but a single coroutine can only wait once (for one event)
    static constexpr size_t tableSize = sys_traits::coroutine_count;
    std::pair<event *, std::coroutine_handle<>> table[tableSize];
    std::bitset<tableSize> used;
};
} // namespace cppos

#undef LCHAR
#undef LINT
#undef LINTS
#undef DBG_UART0
#undef DBG_UART1
#endif /* CPPOS_CORO_INCLUDED */
