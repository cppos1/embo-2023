#pragma once
#ifndef CPPOS_EV_QUEUE_HH_INCLUDED
#define CPPOS_EV_QUEUE_HH_INCLUDED

#include <cppos/scheduler.hh>

#include <cppos/queue.hh>  // queue_op_status
//#include <utility>
//#include <memory>
#include <atomic>

void debug();

#define DBG_UART0 0
#define DBG_UART1 0
#if DBG_UART0
void dbgChar(char val);
void dbgInt(int line, unsigned val);
void dbgIntSync(int line, unsigned val);
#define LINT(x) dbgInt(__LINE__, x)
#define LINTS(x) dbgIntSync(__LINE__, x)
#define LCHAR(c) (dbgChar(c))
#elif DBG_UART1
void uart1Int(int line, unsigned val);
void uart1IntSync(int line, unsigned val);
void uart1Char(char val);
#define LINT(x) uart1Int(__LINE__, x)
#define LINTS(x) uart1IntSync(__LINE__, x)
#define LCHAR(c) (uart1Char(c))
#else
#define LCHAR(c) (void)0
#define LINT(x) (void)0
#define LINTS(x) (void)0
#endif

namespace cppos
{

// for now we do single ended queues only
// we have a queue body with no public member functions
// we have push and pull ends templated on the specific executor
// the executor must provide a block() member function
template <typename Value, size_t Size, class Executor> class EvQueuePull;
template <typename Value, size_t Size, class Executor> class EvQueuePush;
template <typename Value, size_t Size, class EvLoop> class EvQueueCoPull;
template <typename Value, size_t Size, class EvLoop> class EvQueueCoPush;

template <typename Value, size_t Size>
class EvQueueBody
{
    EvQueueBody(const EvQueueBody&) = delete;
    EvQueueBody& operator =(const EvQueueBody&) = delete;
    //typedef port::interrupt_lock Lock;

public:
    typedef Value value_type;

    explicit EvQueueBody()
      : cnt{0}
      , head{0}
      , tail{0}
      , closed{false}
    {}

    ~EvQueueBody() noexcept = default;

private:
#if 1 // ToDo: think about yield
    template <class Executor>
    void close() noexcept
    {
        bool wasClosed = closed.exchange(true);
        if (wasClosed)
        {
            return;
        }

        if (notEmpty.any())
        {
            // we're single-ended, so notify_one is ok
            notEmpty.notify_one<Executor>();
        }

        if (notFull.any())
        {
            notEmpty.notify_one<Executor>();
        }
    }
#endif

#if 0
    queue_op_status wait_pop(Value &v)
    {
        queue_op_status ret = queue_op_status::success;
        bool irqs_enabled = port::interrupts_enabled();
        Lock l;

        while (cnt == 0 && !closed)
        {
            @@@ incomplete: still to do down here
            notEmpty.wait(l);
        }

        if (cnt != 0)
        {
            v = std::move(buf[head]);
            --cnt;
            head = (head+1) % Size;
        }
        else
        {
            ret = queue_op_status::closed;
        }

        l.unlock();
        notFull.notify_one();

        return ret;
    }

    queue_op_status try_pop(Value &v)
    {
        Lock l(mtx, std::try_to_lock);

        if (!l)
        {
            return queue_op_status::would_block;
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == 0)
        {
            return queue_op_status::empty;
        }

        v = std::move(buf[head]);
        --cnt;
        head = (head+1) % Size;

        l.unlock();
        notFull.notify_one();

        return queue_op_status::success;
    }

    //void push(const Value& v);
    template <typename Vt>
    queue_op_status wait_push(Vt &&v)
    {
        Lock l(mtx);

        while (cnt == Size && !closed)
        {
            notFull.wait(l);
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        buf[tail] = std::forward<Vt>(v);
        ++cnt;
        tail = (tail+1) % Size;

        l.unlock();
        notEmpty.notify_one();

        return queue_op_status::success;
    }


    template <typename Vt>
    queue_op_status try_push(Vt &&v)
    {
        Lock l(mtx, std::try_to_lock);
        if (!l)
        {
            return queue_op_status::would_block;
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == Size)
        {
            return queue_op_status::full;
        }

        buf[tail] = std::forward<Vt>(v);
        ++cnt;
        tail = (tail+1) % Size;

        l.unlock();
        notEmpty.notify_one();

        return queue_op_status::success;
    }
#endif

private:
    friend void ::debug();

    template <typename V, size_t S, class Executor>
    friend class EvQueuePull;
    template <typename V, size_t S, class Executor>
    friend class EvQueuePush;
    template <typename V, size_t S, class EvLoop>
    friend class EvQueueCoPull;
    template <typename V, size_t S, class EvLoop>
    friend class EvQueueCoPush;

    Value buf[Size];

    event notFull;
    event notEmpty;

    std::atomic<size_t> cnt;
    size_t head;
    size_t tail;
    std::atomic<bool> closed;
};

template <typename Value, size_t Size, class Executor>
class EvQueuePull
{
public:
    EvQueuePull(EvQueueBody<Value, Size> &body_)
      : body(body_)
    {
    }

#if 1 // ToDo: think about yield
    void close()
    {
        body.template close<Executor>();
    }
#endif

    queue_op_status wait_pop(Value &v)
    {
        queue_op_status ret = queue_op_status::success;

        // we're single ended, so cnt can't change back to 0 if it was >0
        size_t localCnt;
        while ((localCnt = body.cnt) == 0 && !body.closed)
        {
            Executor::block(body.notEmpty);
        }

        if (localCnt != 0)
        {
            v = std::move(body.buf[body.head]);
            --body.cnt;
            body.head = (body.head+1) % Size;
        }
        else
        {
            ret = queue_op_status::closed;
        }

        body.notFull.template notify_one<Executor>();

        return ret;
    }

    queue_op_status try_pop(Value &v)
    {
        if (body.cnt.load() == 0)
        {
            if (body.closed)
            {
                return queue_op_status::closed;
            }
            else
            {
                return queue_op_status::empty;
            }
        }

        v = std::move(body.buf[body.head]);
        --body.cnt;
        body.head = (body.head+1) % Size;

        body.notFull.template notify_one<Executor>();

        return queue_op_status::success;
    }

private:
    friend void ::debug();

    EvQueueBody<Value, Size> &body;
};


template <typename Value, size_t Size, class Executor>
class EvQueuePush
{
public:
    EvQueuePush(EvQueueBody<Value, Size> &body_)
      : body(body_)
    {
    }

#if 1 // ToDo: think about yield
    void close()
    {
        body.template close<Executor>();
    }
#endif

    queue_op_status wait_push(Value &v)
    {
        // we're single ended, so cnt can't change back to full if it was <full
        size_t localCnt;
        bool localClosed = false;
        while ((localCnt = body.cnt) == Size
               && (localClosed = body.closed) == false)
        {
            Executor::block(body.notFull);
        }

        if (localClosed)
        {
            return queue_op_status::closed;
        }


        body.buf[body.tail] = std::forward<Value>(v);
        ++body.cnt;
        body.tail = (body.tail+1) % Size;

        // ToDo: we may need to schedule, but not if inside an ISR
        body.notEmpty.template notify_one<Executor>();

        return queue_op_status::success;
    }

    template <typename Vt>
    queue_op_status try_push(Vt &&v)
    {
        if (body.closed.load())
        {
            return queue_op_status::closed;
        }

        if (body.cnt == Size)
        {
            return queue_op_status::full;
        }

        body.buf[body.tail] = std::forward<Value>(v);
        ++body.cnt;
        body.tail = (body.tail+1) % Size;

        body.notEmpty.template notify_one<Executor>();

        return queue_op_status::success;
    }

private:
    friend void ::debug();

    EvQueueBody<Value, Size> &body;
};



template <class EvLoop, class Pred, class F>
class EvAwaiter
{
    typedef std::coroutine_handle<> coroHandleT;

public:
    EvAwaiter(EvLoop &el, event &e, Pred evCond, F resultProvider)
      : ev(e)
      , loop(el)
      , cond(evCond)
      , resulter(resultProvider)
    {
    }

    ~EvAwaiter()
    {
        if (coro)
        {
            loop.remove(ev, coro);
        }
    }

    bool await_ready() const noexcept
    {
        return cond();
    }

    bool await_suspend(coroHandleT self) noexcept
    {
        if (cond())
        {
            return false;
        }
        else
        {
            coro = self;
            LINT(ev.id());

            loop.add(ev, coro);
            return true; // stay suspended
        }
    }

    auto await_resume() noexcept
    {
        loop.remove(ev, coro);
        coro = nullptr;
        return resulter();
    }


private:
    event &ev;
    EvLoop &loop;
    coroHandleT coro;
    Pred cond;
    F resulter;
};

template <typename Value, size_t Size, class EvLoop>
class EvQueueCoPush
{
public:
    EvQueueCoPush(EvQueueBody<Value, Size> &body_, EvLoop &l)
      : body(body_)
      , loop(l)
    {
    }

#if 1
    void close()
    {
        body.template close<typename EvLoop::executor_type>();
    }
#endif

    auto co_push(Value &v)
    {
        auto readyCond = [&b = body]
                         {
                             return b.cnt < Size || b.closed;
                         };

        auto resulter = [&v, &b = body]
                        {
                            if (b.closed)
                            {
                                return queue_op_status::closed;
                            }
                            // we don't handle the full case here
                            // for single ended queues this is good enough
                            // for multiple providers we need another level
                            // of indirection, as we can't suspend from
                            // await_resume()...
                            b.buf[b.tail] = std::forward<Value>(v);
                            ++b.cnt;
                            b.tail = (b.tail+1) % Size;

                            b.notEmpty.template notify_one<typename EvLoop::executor_type>();

                            return queue_op_status::success;
                        };

        return EvAwaiter(loop,
                         body.notFull,
                         readyCond,
                         resulter);
    }

private:
    friend void ::debug();

    EvQueueBody<Value, Size> &body;
    EvLoop &loop;
};

template <typename Value, size_t Size, class EvLoop>
class EvQueueCoPull
{
public:
    EvQueueCoPull(EvQueueBody<Value, Size> &body_, EvLoop &l)
      : body(body_)
      , loop(l)
    {
    }

#if 1
    void close()
    {
        body.template close<EvLoop::executor_type>();
    }
#endif

    auto co_pop(Value &v)
    {
        auto readyCond = [&b = body]
                         {
                             return b.cnt != 0 || b.closed;
                         };

        auto resulter = [&v, &b = body]
                        {
                            if (b.closed)
                            {
                                return queue_op_status::closed;
                            }

                            v = std::move(b.buf[b.head]);
                            --b.cnt;
                            b.head = (b.head+1) % Size;

                            b.notFull.template notify_one<typename EvLoop::executor_type>();

                            return queue_op_status::success;
                        };

        return EvAwaiter(loop,
                         body.notEmpty,
                         readyCond,
                         resulter);
    }

private:
    friend void ::debug();

    EvQueueBody<Value, Size> &body;
    EvLoop &loop;
};
} // namespace cppos

#undef LCHAR
#undef LINT
#undef LINTS
#undef DBG_UART0
#undef DBG_UART1
#endif /* CPPOS_EV_QUEUE_HH_INCLUDED */
