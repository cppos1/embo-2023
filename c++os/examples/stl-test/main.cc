// very simple STL example


#include <string>
#include <cstring> // memcpy
#include <vector>
#include <memory_resource>
#include <charconv>

#include <outStream.hh>

#include <ex-helpers.hh>

using namespace std::literals;


namespace
{
uint8_t buf[80] = "Here's the board\r\n";

class StaticAllocator : public std::pmr::memory_resource
{
public:
    StaticAllocator(std::byte *p, size_t s) noexcept
      : buf(p),
        restSize(s)
    {}

private:
    void* do_allocate(std::size_t s, std::size_t algn) noexcept override
    {
        void *p = buf;
        if (s < restSize)
        {
            buf += s;
            restSize += s;
        }
        else
        {
            p = nullptr;
        }

        return p;
    }

    void do_deallocate(void* p, std::size_t s, std::size_t algn) noexcept override
    {
    }

    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override
    {
        return true;
    }

    std::byte *buf;
    size_t restSize;
};

constexpr size_t pmrSize = 1024;
std::byte pmrBuf[pmrSize];
StaticAllocator alloc(pmrBuf, pmrSize);

std::pmr::vector<int> vi({10, -24, 3456}, &alloc);
std::pmr::vector<std::pmr::string> vs(&alloc);


void fillVect()
{
    vi.push_back(-713);

    vs.push_back(std::pmr::string("Hello", &alloc));
    vs.push_back(std::pmr::string(" AVR", &alloc));
}

void printVect()
{
    auto end = (char *)buf;
    auto bufEnd = end + 80;
    for (int const &i: vi)
    {
        end = std::to_chars(end, bufEnd, i).ptr;
        *(end++) = ',';
    }

    *(end++) = ' ';
    for (std::pmr::string const &s: vs)
    {
        memcpy(end, s.data(), s.length());
        end += s.length();
        *(end++) = ',';
    }

    *(end++) = '\r';
    *(end++) = '\n';
    uint8_t len = ((uint8_t volatile *)end) - buf;
    dbgOut.write(buf, len);

    while (!(dbgOut.done()))
    {
        //
    }
}
} // unnamed namespace

extern "C"
{
[[noreturn]] void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"

namespace std
{
[[noreturn]] void __throw_length_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_logic_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_bad_array_new_length()
{
    error_handler();
}

std::pmr::memory_resource::~memory_resource()
{
}
} // namespace std

void operator delete(void*, unsigned int)
{
}

int main( void )
{
    no_sched_setup();

    fillVect();
    printVect();

    return 0;
}
