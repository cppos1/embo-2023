#include <stdint.h>

namespace
{
uint8_t hexChar[] = "0123456789abcdef";
} // unnamed namespace

uint8_t volatile *uint32ToHex(uint32_t val, uint8_t volatile *start)
{
    *(start++) = hexChar[(val >> 28) & 0xf];
    *(start++) = hexChar[(val >> 24) & 0xf];
    *(start++) = hexChar[(val >> 20) & 0xf];
    *(start++) = hexChar[(val >> 16) & 0xf];
    *(start++) = hexChar[(val >> 12) & 0xf];
    *(start++) = hexChar[(val >> 8) & 0xf];
    *(start++) = hexChar[(val >> 4) & 0xf];
    *(start++) = hexChar[(val >> 0) & 0xf];

    return start;
}

uint8_t volatile *uint8ToHex(uint8_t val, uint8_t volatile *start)
{
    *(start++) = hexChar[(val >> 4) & 0xf];
    *(start++) = hexChar[(val >> 0) & 0xf];

    return start;
}

