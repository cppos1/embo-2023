// simple debug output via UART

#ifndef OUT_STREAM_HH_SEEN
#define OUT_STREAM_HH_SEEN
#include <stdint.h>
#include <avr/io.h>

#ifndef USART_UDRE_vect
#define USART_UDRE_vect USART0_UDRE_vect
#define USART_TX_vect USART0_TX_vect
#endif

extern "C"
{
[[gnu::signal,gnu::used,gnu::externally_visible]] void USART_UDRE_vect();
[[gnu::signal,gnu::used,gnu::externally_visible]] void USART_TX_vect();
} // extern "C"

class OutStream
{
public:
    OutStream();
    ~OutStream();

    void write(void const *data, uint16_t len);
    void reset();
    bool done() const;

private:
    friend void ::USART_UDRE_vect();
    friend void ::USART_TX_vect();

    enum IoState : uint8_t
    {
        idle,
        txBusy,
        txDone,
        rxBusy,
        rxDone,
    };

    static constexpr uint16_t uartBaud = 19200;
    static constexpr uint16_t maxLen = 80;

    void sendNext();

    uint8_t buf[maxLen];
    volatile IoState txState = idle;
    uint32_t txCurrent = 0;
    uint32_t txLen = 0;
};

extern OutStream dbgOut;
#endif /* OUT_STREAM_HH_SEEN */
