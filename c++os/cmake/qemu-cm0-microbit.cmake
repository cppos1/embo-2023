if(NOT _INCLUDED_TOOLCHAIN_FILE )
  # the port to use
  # for now this needs to be a subdirectory of cppos/src/ports
  set(CPPOS_PORT cortex-m0)
  # for now this needs to be a subdirectory of cppos/include/cppos/ports
  set(CPPOS_PORTHEADER cortex-m0)

  # directory for port of helpers for examples
  # for now this needs to be a subdirectory of cppos/examples/ports
  #set(CPPOS_EXAMPLES_PORT avr) // we don't have one yet for this board


  include(${CMAKE_CURRENT_LIST_DIR}/cm0-toolchain.cmake)

  list(APPEND CPPOS_LATE_LIBRARIES -Wl,--start-group -lgcc -lc -Wl,--end-group)

  # for now don't try_compile
  set(CMAKE_CXX_COMPILER_FORCED TRUE)
  set(CMAKE_C_COMPILER_FORCED TRUE)

  function(cppos_extra_board_setup TARGET)
    target_sources(${TARGET} PUBLIC
      ${CPPOS_SUPPORT_DIR}/qemu-cm0-microbit/startup.S
    )
    target_link_options(${TARGET} PUBLIC -T${CPPOS_SUPPORT_DIR}/qemu-cm0-microbit/nrf5.ld)
    target_link_options(${TARGET} PUBLIC -nostdlib -nostartfiles)

    # find SDK directory
    find_path(NRF5_SDK_COMPONENTS_DIR device/nrf51.h PATHS /components NO_SYSTEM_ENVIRONMENT_PATH REQUIRED)
    include_directories(${NRF5_SDK_COMPONENTS_DIR})

    # find CMSIS directory
    find_path(CMSIS_CORE_DIR core_cm0.h PATHS /CMSIS/Core/Include REQUIRED)
    include_directories(${CMSIS_CORE_DIR})

    include_directories(${CPPOS_SUPPORT_DIR}/qemu-cm0-microbit)

  endfunction()
endif()
