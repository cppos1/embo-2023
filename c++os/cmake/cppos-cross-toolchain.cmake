# common settings for cross toolchain for C++OS

set(CMAKE_SYSTEM_NAME CPPOS)
set(CMAKE_CROSSCOMPILING 1)

find_program(CPPOS_CC ${CROSS_PREFIX}gcc${COMPILER_SUFFIX} REQUIRED)
find_program(CPPOS_CXX ${CROSS_PREFIX}g++${COMPILER_SUFFIX} REQUIRED)
find_program(CPPOS_OBJCOPY ${CROSS_PREFIX}objcopy REQUIRED)
find_program(CPPOS_SIZE_TOOL ${CROSS_PREFIX}size REQUIRED)
find_program(CPPOS_OBJDUMP ${CROSS_PREFIX}objdump REQUIRED)

set(CMAKE_C_COMPILER ${CPPOS_CC})
set(CMAKE_ASM_COMPILER ${CPPOS_CC})
set(CMAKE_CXX_COMPILER ${CPPOS_CXX})
set(CMAKE_OBJDUMP ${CPPOS_OBJDUMP})
set(CMAKE_OBJCOPY ${CPPOS_OBJCOPY})

set(CMAKE_EXECUTABLE_SUFFIX .elf)

# set any HW/ABI related options before invoking the compiler
list(APPEND CPPOS_COMMON_OPTIONS -fno-rtti -fno-exceptions -fno-use-cxa-atexit)

# some heuristic to get the root of the cross environment
# if the heuristic fails, set CMAKE_FIND_ROOT_PATH in your board or project file
execute_process(
  COMMAND ${CPPOS_CC} ${CPPOS_COMMON_OPTIONS} -print-file-name=libc.a
  OUTPUT_VARIABLE CPPOS_CROSS_LIBC_PATH
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
)
file(REAL_PATH ${CPPOS_CROSS_LIBC_PATH} CPPOS_CROSS_LIBC_NORM_PATH)
cmake_path(GET CPPOS_CROSS_LIBC_NORM_PATH PARENT_PATH CPPOS_CROSS_LIBC_DIR)

file(REAL_PATH ${CPPOS_CROSS_LIBC_DIR}/.. CPPOS_CROSS_ROOT)

# now we add the found directory to CMAKE_FIND_ROOT_PATH
list(APPEND CMAKE_FIND_ROOT_PATH ${CPP_CROSS_ROOT})

# we want to search for includes and libraries only in our root
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug)
endif()
add_compile_options(-Wno-volatile -Wno-array-bounds -Wall -g)
if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
  add_compile_options(-g -Og)
else()
  add_compile_options(-g -Os)
endif()

add_compile_options(${CPPOS_COMMON_OPTIONS})

add_link_options(${CPPOS_COMMON_OPTIONS})

#set(CMAKE_ASM_FLAGS ${CMAKE_ASM_FLAGS} -Wa,--gstabs) # we use '-g'

# find libgcc dir
execute_process(
  COMMAND ${CPPOS_CC} ${CPPOS_COMMON_OPTIONS} -print-libgcc-file-name
  OUTPUT_VARIABLE CPPOS_CROSS_LIBGCC_PATH
  OUTPUT_STRIP_TRAILING_WHITESPACE
  COMMAND_ERROR_IS_FATAL ANY
)
file(REAL_PATH ${CPPOS_CROSS_LIBGCC_PATH} CPPOS_CROSS_LIBGCC_NORM_PATH)
cmake_path(GET CPPOS_CROSS_LIBGCC_NORM_PATH PARENT_PATH CPPOS_CROSS_LIBGCC_DIR)

include_directories(${CMAKE_SOURCE_DIR}) # for systraits.hh
set(CPPOS_PORT_INC_DIR ${CPPOS_INC_DIR}/cppos/ports/${CPPOS_PORTHEADER})
