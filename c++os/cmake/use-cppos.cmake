# settings for C++OS projects

set(CPPOS_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
file(REAL_PATH ${CMAKE_CURRENT_LIST_DIR}/.. CPPOS_TOP_DIR)
set(CPPOS_INC_DIR ${CPPOS_TOP_DIR}/include)
set(CPPOS_SUPPORT_DIR ${CPPOS_TOP_DIR}/support)

if (NOT (DEFINED CMAKE_TOOLCHAIN_FILE OR DEFINED BOARD))
  message(FATAL_ERROR "either BOARD must be defined or a toolchain file given on the command line")
endif()

if (NOT (DEFINED CMAKE_TOOLCHAIN_FILE))
  if (NOT EXISTS ${CPPOS_CMAKE_DIR}/${BOARD}.cmake)
    message(FATAL_ERROR "unknown BOARD ${BOARD}")
  endif()
endif()

#include(${CPPOS_CMAKE_DIR}/${BOARD}.cmake)
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/${BOARD}.cmake)
list(APPEND CMAKE_MODULE_PATH ${CPPOS_CMAKE_DIR})

enable_language(ASM)

function(use_cppos TARGET)
  cppos_extra_board_setup(${TARGET})
  cppos_extra_target_setup(${TARGET})
  #include(${CPPOS_CMAKE_DIR}/checks.cmake)
  add_subdirectory(${CPPOS_TOP_DIR}/src c++os)
  target_sources(${TARGET} PRIVATE $<TARGET_OBJECTS:c++OsPortLib>)
  target_link_libraries(${TARGET} c++OsLib  ${CPPOS_LATE_LIBRARIES})
  target_include_directories(${TARGET} PUBLIC
    ${CPPOS_INC_DIR}/std
    ${CPPOS_PORT_INC_DIR}
    ${CPPOS_INC_DIR})
endfunction()

set(CPPOS_LST_OPTIONS -h -S -w --show-all-symbols -t)
set(CPPOS_BIN_OPTIONS -O binary)
set(CPPOS_HEX_OPTIONS -O ihex)
function(cppos_add_extra_outputs TARGET)
  # lst
  add_custom_command(
    TARGET ${TARGET}
    POST_BUILD
    COMMAND ${CMAKE_OBJDUMP} ${CPPOS_LST_OPTIONS} ${TARGET}.elf > ${TARGET}.lst
  )
  # bin
  add_custom_command(
    TARGET ${TARGET}
    POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} ${CPPOS_BIN_OPTIONS} ${TARGET}.elf ${TARGET}.bin
  )
  # hex
  add_custom_command(
    TARGET ${TARGET}
    POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} ${CPPOS_HEX_OPTIONS} ${TARGET}.elf ${TARGET}.hex
  )
  # map
  target_link_options(${TARGET} PUBLIC -Wl,-Map=${TARGET}.map)
endfunction()
