
set(CMAKE_SYSTEM_PROCESSOR avr)

# the default tools prefix
set(CROSS_PREFIX avr-)

add_compile_options(-DF_CPU=${CPPOS_CLOCK})

set(CPPOS_COMMON_OPTIONS -mmcu=${CPPOS_MCU})

# any HW/ABI related options must be set before this
include(${CMAKE_CURRENT_LIST_DIR}/cppos-cross-toolchain.cmake)

function(cppos_extra_target_setup TARGET)
  if(DEFINED SIMUL_OUT_ADDR)
    target_compile_options(${TARGET} PUBLIC "-DSIMOUT(x)=(*((volatile char *)(${SIMUL_OUT_ADDR} + 0x20)))=(x)")
  else()
    target_compile_options(${TARGET} PUBLIC "-DSIMOUT(x)=SIMOUT(x)=(void)0")
  endif()
endfunction()
