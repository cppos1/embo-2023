if(NOT _INCLUDED_TOOLCHAIN_FILE )
  # the port to use
  # for now this needs to be a subdirectory of cppos/src/ports
  set(CPPOS_PORT rp2040)
  # for now this needs to be a subdirectory of cppos/include/cppos/ports
  set(CPPOS_PORTHEADER rp2040)

  # directory for port of helpers for examples
  # for now this needs to be a subdirectory of cppos/examples/ports
  #set(CPPOS_EXAMPLES_PORT avr) // we don't have one yet for this board


  include(${CMAKE_CURRENT_LIST_DIR}/cm0+-toolchain.cmake)

  list(APPEND CPPOS_LATE_LIBRARIES -Wl,--start-group -lgcc -lc -Wl,--end-group)

  # for now don't try_compile
  set(CMAKE_CXX_COMPILER_FORCED TRUE)
  set(CMAKE_C_COMPILER_FORCED TRUE)

  add_compile_definitions(__RP2040_PORT)

  function(cppos_extra_board_setup TARGET)
    target_sources(${TARGET} PUBLIC
      ${CPPOS_SUPPORT_DIR}/rp-pico/startup.S
    )
    target_link_options(${TARGET} PUBLIC -T${CPPOS_SUPPORT_DIR}/rp-pico/pico.ld)
    target_link_options(${TARGET} PUBLIC -nostdlib -nostartfiles)

    # find SDK directory
    find_file(PICO_CHECKSUM_TOOL src/rp2_common/boot_stage2/pad_checksum PATHS / NO_SYSTEM_ENVIRONMENT_PATH REQUIRED)
    # boot stage 2 loader
    add_executable(bs2-base ${CPPOS_SUPPORT_DIR}/rp-pico/boot2_w25q080.S)
    target_link_options(bs2-base PUBLIC -nostartfiles --specs=nosys.specs)
    target_link_options(bs2-base PUBLIC -T${CPPOS_SUPPORT_DIR}/rp-pico/boot_stage2.ld)
    add_custom_command(
      OUTPUT bs2-base.bin
      COMMAND ${CMAKE_OBJCOPY} ${CPPOS_BIN_OPTIONS} bs2-base.elf bs2-base.bin
      DEPENDS bs2-base
    )
    add_custom_command(
      OUTPUT bs2-complete.S
      COMMAND ${PICO_CHECKSUM_TOOL} bs2-base.bin -s 0xffffffff bs2-complete.S
      DEPENDS bs2-base.bin
    )
    target_sources(${TARGET} PUBLIC bs2-complete.S)

    # find CMSIS directory
    find_path(CMSIS_CORE_DIR core_cm0.h PATHS /CMSIS/Core/Include REQUIRED)
    include_directories(${CMSIS_CORE_DIR})

    include_directories(${CPPOS_SUPPORT_DIR}/rp-pico)

  endfunction()
endif()
