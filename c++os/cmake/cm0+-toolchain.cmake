
set(CMAKE_SYSTEM_PROCESSOR arm-cm0p)

# the chip
set(CPPOS_MCU cortex-m0plus)

# the default tools prefix
set(CROSS_PREFIX arm-none-eabi-)

set(CPPOS_COMMON_OPTIONS -mthumb -mcpu=${CPPOS_MCU})

# any HW/ABI related options must be set before this
include(${CMAKE_CURRENT_LIST_DIR}/cppos-cross-toolchain.cmake)

add_link_options(${TARGET} -Wl,--cref,--gc-sections,--no-warn-mismatch)


function(cppos_extra_target_setup TARGET)
endfunction()
